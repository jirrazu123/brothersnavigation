-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 08, 2019 at 01:34 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.1.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `brnav`
--

-- --------------------------------------------------------

--
-- Table structure for table `agencies`
--

CREATE TABLE `agencies` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` text,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `contact_person` varchar(255) DEFAULT NULL,
  `contact_no` varchar(255) DEFAULT NULL,
  `current_balance` double DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `initail_balance` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `agencies`
--

INSERT INTO `agencies` (`id`, `name`, `address`, `email`, `phone`, `contact_person`, `contact_no`, `current_balance`, `status`, `created_at`, `updated_at`, `deleted_at`, `initail_balance`) VALUES
(1, 'Halim Ahmad', 'Dhaka -1000', 'halim@gmil.com', '0175468245', 'Habib Ahmad', '01857198657', 6000, NULL, '2019-04-18 08:54:10', '2019-04-18 09:53:18', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `basic_settings`
--

CREATE TABLE `basic_settings` (
  `id` int(11) NOT NULL,
  `company_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `logo` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `address` text CHARACTER SET utf8 NOT NULL,
  `contact` varchar(255) CHARACTER SET utf8 NOT NULL,
  `status` tinyint(2) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `basic_settings`
--

INSERT INTO `basic_settings` (`id`, `company_name`, `logo`, `address`, `contact`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Brother\'s Navigation', '3.jpg', '37/2, Protima Jaman Tower, Room No 703(six floor) Box CAlvart Road, Purona Polton, Dhaka.', '02-57160540', NULL, NULL, '2019-04-09 05:58:22', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cargos`
--

CREATE TABLE `cargos` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `capacity` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cargos`
--

INSERT INTO `cargos` (`id`, `name`, `capacity`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Rustom 05', '40000 kg', 1, '2019-04-18 06:30:09', '2019-04-18 07:09:20', '2019-04-18 12:30:09');

-- --------------------------------------------------------

--
-- Table structure for table `cargo_collections`
--

CREATE TABLE `cargo_collections` (
  `id` int(11) NOT NULL,
  `cargo_trip_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cargo_expenses`
--

CREATE TABLE `cargo_expenses` (
  `id` int(11) NOT NULL,
  `cargo_trip_id` int(11) DEFAULT NULL,
  `head_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `status` tinyint(4) DEFAULT '1',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cargo_expenses`
--

INSERT INTO `cargo_expenses` (`id`, `cargo_trip_id`, `head_id`, `amount`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(7, 2, 1, 325, 1, '2019-04-10 13:55:35', '2019-04-10 13:55:35', '2019-04-10 19:55:35'),
(8, 2, 3, 7878, 1, '2019-04-10 13:55:35', '2019-04-10 13:55:35', '2019-04-10 19:55:35'),
(11, NULL, 1, 456, 1, '2019-04-23 10:49:56', '2019-04-23 10:49:56', '2019-04-23 16:49:56'),
(12, NULL, 1, 456, 1, '2019-04-23 10:50:25', '2019-04-23 10:50:25', '2019-04-23 16:50:25'),
(13, NULL, 1, 456, 1, '2019-04-23 10:50:45', '2019-04-23 10:50:45', '2019-04-23 16:50:45'),
(14, NULL, 3, 450, 1, '2019-04-23 10:50:45', '2019-04-23 10:50:45', '2019-04-23 16:50:45'),
(18, 8, 1, 500, 1, '2019-04-23 11:46:55', '2019-04-23 11:46:55', '2019-04-23 17:46:55'),
(19, 8, 1, 500, 1, '2019-04-23 11:48:46', '2019-04-23 11:48:46', '2019-04-23 17:48:46'),
(20, 8, 3, 450, 1, '2019-04-23 11:48:46', '2019-04-23 11:48:46', '2019-04-23 17:48:46'),
(21, 8, 1, 500, 1, '2019-04-23 11:49:04', '2019-04-23 11:49:04', '2019-04-23 17:49:04'),
(22, 8, 3, 450, 1, '2019-04-23 11:49:04', '2019-04-23 11:49:04', '2019-04-23 17:49:04'),
(23, 7, 1, 5000, 1, '2019-04-24 06:55:24', '2019-04-24 06:55:24', '2019-04-24 12:55:24'),
(24, 7, 3, 4000, 1, '2019-04-24 06:55:24', '2019-04-24 06:55:24', '2019-04-24 12:55:24'),
(27, 7, 3, 500, 1, '2019-04-24 13:08:37', '2019-04-24 13:08:37', '2019-04-24 19:08:37'),
(28, 7, 4, 4500, 1, '2019-04-24 13:08:37', '2019-04-24 13:08:37', '2019-04-24 19:08:37'),
(41, 1, 1, 500, 1, '2019-04-24 14:17:56', '2019-04-24 14:17:56', '2019-04-24 20:17:56'),
(42, 1, 1, 500, 1, '2019-04-24 14:17:57', '2019-04-24 14:17:57', '2019-04-24 20:17:57'),
(43, 1, 1, 200, 1, '2019-04-24 14:17:57', '2019-04-24 14:17:57', '2019-04-24 20:17:57'),
(44, 7, 1, 500, 1, '2019-04-24 14:20:04', '2019-04-24 14:20:04', '2019-04-24 20:20:04'),
(45, 7, 4, 800, 1, '2019-04-24 14:20:05', '2019-04-24 14:20:05', '2019-04-24 20:20:05');

-- --------------------------------------------------------

--
-- Table structure for table `cargo_expense_heads`
--

CREATE TABLE `cargo_expense_heads` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cargo_expense_heads`
--

INSERT INTO `cargo_expense_heads` (`id`, `name`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Ashikur', 1, '0000-00-00 00:00:00', '2019-04-18 08:18:30', '0000-00-00 00:00:00'),
(3, 'Md Pirvaz', 0, '2019-04-10 08:08:46', '2019-04-10 08:08:46', '2019-04-10 14:08:46'),
(4, 'Desial and Mobile', 1, '2019-04-24 12:22:01', '2019-04-24 12:39:42', '2019-04-24 18:22:01');

-- --------------------------------------------------------

--
-- Table structure for table `cargo_trips`
--

CREATE TABLE `cargo_trips` (
  `id` int(11) NOT NULL,
  `trip_auto_id` varchar(255) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `cargo_id` int(11) NOT NULL,
  `agency_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `weight` double NOT NULL,
  `rate` double NOT NULL,
  `des_form` varchar(255) NOT NULL,
  `des_to` varchar(255) NOT NULL,
  `selling_date` datetime NOT NULL,
  `unload_date` datetime NOT NULL,
  `is_expense` tinyint(2) NOT NULL DEFAULT '0',
  `total_expense` double DEFAULT NULL,
  `is_damarage` tinyint(4) DEFAULT '0',
  `status` tinyint(4) DEFAULT NULL,
  `demurrage_id` int(11) DEFAULT NULL,
  `demurrage_amount` double DEFAULT NULL,
  `demurrage_date` timestamp NULL DEFAULT NULL,
  `demurrage_received_date` timestamp NULL DEFAULT NULL,
  `damarage_status` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cargo_trips`
--

INSERT INTO `cargo_trips` (`id`, `trip_auto_id`, `customer_id`, `cargo_id`, `agency_id`, `product_id`, `weight`, `rate`, `des_form`, `des_to`, `selling_date`, `unload_date`, `is_expense`, `total_expense`, `is_damarage`, `status`, `demurrage_id`, `demurrage_amount`, `demurrage_date`, `demurrage_received_date`, `damarage_status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '', 1, 1, 1, 1, 60000, 12, 'Dhaka', 'Borishal', '2019-04-23 00:00:00', '2019-04-23 00:00:00', 1, 1200, 0, 1, NULL, NULL, NULL, NULL, NULL, '2019-04-21 06:08:04', '2019-04-24 14:17:57', '2019-04-21 12:08:04'),
(7, '', 13, 1, 1, 1, 80000, 6, 'Borishal', 'Dhaka', '2019-04-21 00:00:00', '2019-04-22 00:00:00', 1, 1300, 0, 1, NULL, NULL, NULL, NULL, NULL, '2019-04-21 07:20:57', '2019-04-24 14:20:05', '2019-04-21 13:20:57'),
(8, '', 3, 1, 1, 1, 40000, 12, 'Dhaka', 'Dhaka', '2019-04-22 00:00:00', '2019-04-23 00:00:00', 0, 0, 0, 1, NULL, NULL, NULL, NULL, NULL, '2019-04-21 09:41:43', '2019-04-23 11:49:04', '2019-04-21 15:41:43'),
(9, 'SOFTID-2019424-0004', 1, 1, 1, 1, 45000, 5, 'Khulna', 'Vola', '2019-04-24 00:00:00', '2019-04-25 00:00:00', 1, 1100, 0, 1, NULL, NULL, NULL, NULL, NULL, '2019-04-24 06:40:39', '2019-04-24 13:15:30', '2019-04-24 12:40:39'),
(10, 'SOFTID-201957-0005', 13, 2, 1, 2, 4000, 50, 'dhaka', 'borishal', '2019-05-06 00:00:00', '2019-05-08 00:00:00', 0, NULL, 0, 1, NULL, 5000, NULL, NULL, NULL, '2019-05-07 13:09:50', '2019-05-07 13:09:50', '2019-05-07 19:09:50'),
(11, 'SOFTID-201957-0006', 13, 1, 1, 1, 40000, 500, 'd', 'b', '2019-05-08 00:00:00', '2019-05-09 00:00:00', 0, NULL, 1, 1, 2, 400, '2019-05-05 18:00:00', '2019-05-06 18:00:00', 1, '2019-05-07 13:27:33', '2019-05-08 11:33:53', '2019-05-07 19:27:33');

-- --------------------------------------------------------

--
-- Table structure for table `collections`
--

CREATE TABLE `collections` (
  `id` int(11) NOT NULL,
  `type_id` tinyint(2) NOT NULL COMMENT '1 for up and 2 for down',
  `trip_id` int(11) DEFAULT NULL,
  `income_head_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `collections`
--

INSERT INTO `collections` (`id`, `type_id`, `trip_id`, `income_head_id`, `amount`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(23, 1, 6, 2, 500, 1, '2019-04-11 04:59:24', '2019-04-11 04:59:24', '2019-04-11 10:59:24'),
(24, 1, 6, 2, 600, 1, '2019-04-11 04:59:24', '2019-04-11 04:59:24', '2019-04-11 10:59:24'),
(25, 2, 6, 2, 600, 1, '2019-04-11 04:59:24', '2019-04-11 04:59:24', '2019-04-11 10:59:24');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_designation` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_phone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_mobile` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_email` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invoice_Address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `address`, `phone`, `mobile`, `email`, `contact_name`, `contact_designation`, `contact_phone`, `contact_mobile`, `contact_email`, `invoice_Address`) VALUES
(8, 'Milestone School & College', NULL, '02-7913437 & 02-58957733', NULL, 'milestonecollege1056@gma', 'JAHANGIR ALAM', 'Store In charge', '02-58957733', NULL, NULL, '44, Gareeb-e-Nawas Avenue, Sector-11, Uttara, Dhaka-1230.'),
(12, 'Milestone School & College', NULL, '02-58957733', '01927549859', 'milestone@gmail.com', 'Milestone School & College', 'Store In-charge', '02-48963437', '01676745090', NULL, 'Milestone School & College\r\nUttara Model Town, Dhaka-1230.'),
(13, 'BMJS-sec 9', NULL, NULL, NULL, NULL, 'Iqbal', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `demurrages`
--

CREATE TABLE `demurrages` (
  `id` int(11) NOT NULL,
  `demurrage_type` varchar(255) CHARACTER SET utf8 NOT NULL,
  `sort_des` text CHARACTER SET utf8 NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `demurrages`
--

INSERT INTO `demurrages` (`id`, `demurrage_type`, `sort_des`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'fdfsdf655496546', 'dfsfsdf', 1, '2019-04-25 10:35:46', '2019-04-25 10:37:03', NULL),
(2, 'Oil Tank', 'Oil tank and side pipe', 1, '2019-04-25 10:37:36', '2019-04-25 10:37:36', NULL),
(3, 'rtfdgydfyrty', 'tytryrt', 1, '2019-04-25 10:45:14', '2019-04-25 10:45:14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `expenses`
--

CREATE TABLE `expenses` (
  `id` int(11) NOT NULL,
  `trip_id` int(11) DEFAULT NULL,
  `head_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `status` tinyint(4) DEFAULT '1',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `expenses`
--

INSERT INTO `expenses` (`id`, `trip_id`, `head_id`, `amount`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(5, 1, 1, 35, 1, '2019-04-10 13:54:20', '2019-04-10 13:54:20', '2019-04-10 19:54:20'),
(6, 1, 3, 35, 1, '2019-04-10 13:54:21', '2019-04-10 13:54:21', '2019-04-10 19:54:21'),
(7, 2, 1, 325, 1, '2019-04-10 13:55:35', '2019-04-10 13:55:35', '2019-04-10 19:55:35'),
(8, 2, 3, 7878, 1, '2019-04-10 13:55:35', '2019-04-10 13:55:35', '2019-04-10 19:55:35'),
(9, 1, 2, 2000, 1, '2019-04-10 14:48:21', '2019-04-10 14:48:21', '2019-04-10 20:48:21'),
(10, 1, 2, 500, 1, '2019-04-10 14:48:21', '2019-04-10 14:48:21', '2019-04-10 20:48:21'),
(11, 1, 1, 45, 1, '2019-04-23 09:40:34', '2019-04-23 09:40:34', '2019-04-23 15:40:34'),
(12, 3, 1, 45, 1, '2019-04-23 10:21:32', '2019-04-23 10:21:32', '2019-04-23 16:21:32');

-- --------------------------------------------------------

--
-- Table structure for table `expense_heads`
--

CREATE TABLE `expense_heads` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `expense_heads`
--

INSERT INTO `expense_heads` (`id`, `name`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Ashikur Rahaman Ashik', 1, '0000-00-00 00:00:00', '2019-04-08 10:34:31', '0000-00-00 00:00:00'),
(4, 'Debashis Roy', 1, '2019-04-25 09:11:04', '2019-04-25 09:11:04', '2019-04-25 15:11:04');

-- --------------------------------------------------------

--
-- Table structure for table `income_heads`
--

CREATE TABLE `income_heads` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `income_heads`
--

INSERT INTO `income_heads` (`id`, `name`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 'Rasal', 1, '2019-04-25 09:05:51', '2019-04-25 09:05:51', '2019-04-25 15:05:51'),
(4, 'Mollah', 1, '2019-04-25 09:06:02', '2019-04-25 09:06:02', '2019-04-25 15:06:02');

-- --------------------------------------------------------

--
-- Table structure for table `managers`
--

CREATE TABLE `managers` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `nid` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `mobile` varchar(255) CHARACTER SET utf8 NOT NULL,
  `photo` varchar(255) CHARACTER SET utf8 NOT NULL,
  `present_address` varchar(255) CHARACTER SET utf8 NOT NULL,
  `permanent_address` varchar(255) CHARACTER SET utf8 NOT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `managers`
--

INSERT INTO `managers` (`id`, `name`, `nid`, `mobile`, `photo`, `present_address`, `permanent_address`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(11, 'Rahaman', '1985124568726', '0172456879', '3.jpg', 'Dhaka', 'Dhaka', 1, '2019-04-09 09:12:36', '2019-04-09 09:12:36', NULL),
(12, 'Rahaman', '199254876854826', '01745812458', '3.jpg', 'ka - 65, Polton, Dhaka-1030', 'Same', 1, '2019-04-09 09:13:32', '2019-04-10 08:19:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(11) NOT NULL,
  `label` varchar(255) CHARACTER SET utf8 NOT NULL,
  `permission_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `label`, `permission_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Create Manager', 'create-manager', NULL, '2019-04-24 10:58:30', NULL),
(4, 'general', 'general-permission', '2019-04-25 12:16:58', '2019-04-25 12:16:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Rustom 55', 0, '2019-04-18 09:57:22', '2019-04-18 10:16:45', '2019-04-18 15:57:22'),
(2, 'Rustom  11', 1, '2019-04-18 09:58:21', '2019-04-18 09:58:21', '2019-04-18 15:58:21'),
(3, 'Rustom  11', 1, '2019-04-18 09:59:13', '2019-04-18 09:59:13', '2019-04-18 15:59:13');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `delete_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`, `delete_at`) VALUES
(1, 'Manager', NULL, NULL, NULL),
(2, 'Admin', '2019-04-24 11:22:37', '2019-04-24 11:34:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_permissions`
--

CREATE TABLE `role_permissions` (
  `id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_permissions`
--

INSERT INTO `role_permissions` (`id`, `permission_id`, `role_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, NULL, NULL, NULL),
(2, 1, 2, NULL, NULL, NULL),
(3, 3, 2, NULL, NULL, NULL),
(4, 3, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ships`
--

CREATE TABLE `ships` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `deluxe` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `cabin` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `dak` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ships`
--

INSERT INTO `ships` (`id`, `name`, `status`, `deluxe`, `cabin`, `dak`, `created_at`, `updated_at`, `deleted_at`) VALUES
(6, 'kornofully 11', 0, '45', '125', '452', '2019-04-08 11:24:21', '2019-04-09 14:37:15', '2019-04-08 17:24:21'),
(7, 'Kornofully 12', 1, '45', '125', '450', '2019-04-09 09:34:55', '2019-04-09 09:34:55', '2019-04-09 15:34:55'),
(8, 'Kornofully 05', 1, '12', '23', '450', '2019-04-10 05:25:39', '2019-04-10 05:25:39', '2019-04-10 11:25:39');

-- --------------------------------------------------------

--
-- Table structure for table `ship_expenses`
--

CREATE TABLE `ship_expenses` (
  `id` int(11) NOT NULL,
  `head_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `status` tinyint(4) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `ship_id` int(10) UNSIGNED DEFAULT NULL,
  `expense_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ship_expenses`
--

INSERT INTO `ship_expenses` (`id`, `head_id`, `amount`, `status`, `created_at`, `updated_at`, `deleted_at`, `ship_id`, `expense_date`) VALUES
(8, 1, 424, 1, NULL, NULL, NULL, 7, '2019-04-01'),
(9, 1, 424, 1, NULL, NULL, NULL, 6, '2019-04-03'),
(10, 1, 200, 1, '2019-04-09 12:21:39', '2019-04-09 12:21:39', NULL, 7, '2019-04-05'),
(11, 3, 500, 1, '2019-04-09 12:22:20', '2019-04-09 12:22:20', NULL, 6, '2019-04-08'),
(12, 1, 456, 1, '2019-04-09 13:25:26', '2019-04-09 13:25:26', NULL, 6, '2019-04-07'),
(14, 3, 456, 1, '2019-04-10 07:03:05', '2019-04-10 07:03:05', NULL, 7, '2019-04-10'),
(15, 1, 424, 1, '2019-04-10 07:06:57', '2019-04-10 07:06:57', NULL, 6, '2019-04-01'),
(19, 3, 55, 1, '2019-04-10 07:27:38', '2019-04-10 07:31:02', NULL, 8, '2003-04-08'),
(20, 1, 500, 1, '2019-04-11 08:04:12', '2019-04-11 08:04:12', NULL, 6, '2019-04-09');

-- --------------------------------------------------------

--
-- Table structure for table `ship_expense_heads`
--

CREATE TABLE `ship_expense_heads` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ship_expense_heads`
--

INSERT INTO `ship_expense_heads` (`id`, `name`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Roby roy Dem', 0, '2019-04-08 12:42:18', '2019-04-10 05:35:31', NULL),
(3, 'Rahaman Haji', 1, '2019-04-10 05:35:54', '2019-04-10 05:35:54', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `trips`
--

CREATE TABLE `trips` (
  `id` int(11) NOT NULL,
  `trip_auto_id` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `ship_id` int(11) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `is_expense` tinyint(2) NOT NULL DEFAULT '0',
  `is_collection` tinyint(2) NOT NULL DEFAULT '0',
  `total_expense` double DEFAULT '0',
  `total_up_collection` double DEFAULT NULL,
  `total_down_collection` double DEFAULT NULL,
  `total_other_income` double DEFAULT NULL,
  `profit` double DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trips`
--

INSERT INTO `trips` (`id`, `trip_auto_id`, `name`, `ship_id`, `start_date`, `end_date`, `is_expense`, `is_collection`, `total_expense`, `total_up_collection`, `total_down_collection`, `total_other_income`, `profit`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '', 'Dhaka- Bola-Dhaka', 3, '2019-04-08 00:00:00', '2019-04-10 00:00:00', 1, 1, 45, 1400, 0, 0, 0, 1, '2019-04-06 09:13:43', '2019-04-23 09:40:34', '2019-04-06 15:13:43'),
(2, '', 'Dhak - Borishal', 2, '2019-04-06 00:00:00', '2019-04-07 00:00:00', 1, 1, 8203, 1200, 1100, 0, 0, 1, '2019-04-06 10:34:16', '2019-04-10 15:15:27', '2019-04-06 16:34:16'),
(3, '', 'Dhak - Poaitakhali', 6, '2019-04-07 00:00:00', '2019-04-08 00:00:00', 1, 0, 45, 1500, 1500, 0, 0, 1, '2019-04-06 10:34:53', '2019-04-23 10:21:32', '2019-04-06 16:34:53'),
(5, 'SOFTID-2019411-0004', 'ashik make a trip', 6, '2019-04-11 00:00:00', '2019-04-12 00:00:00', 0, 0, NULL, NULL, NULL, NULL, NULL, 1, '2019-04-11 04:52:08', '2019-04-11 04:52:08', '2019-04-11 10:52:08'),
(6, 'SOFTID-2019411-0005', 'sdkfvskl;a', 7, '2019-04-11 00:00:00', '2019-04-12 00:00:00', 0, 1, NULL, 1100, 600, NULL, NULL, 1, '2019-04-11 04:57:54', '2019-04-11 04:59:24', '2019-04-11 10:57:54');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `role_id`, `updated_at`) VALUES
(1, 'Jahirul', 'admin@gmail.com', NULL, '$2y$10$nqZgFj268KsETiWKYDxR5.kWXp1H5b.lq3a/Zqnc9ztScXD3fIYk.', NULL, '2019-04-04 02:43:46', 1, '2019-04-04 02:43:46');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agencies`
--
ALTER TABLE `agencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `basic_settings`
--
ALTER TABLE `basic_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cargos`
--
ALTER TABLE `cargos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cargo_collections`
--
ALTER TABLE `cargo_collections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cargo_expenses`
--
ALTER TABLE `cargo_expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cargo_expense_heads`
--
ALTER TABLE `cargo_expense_heads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cargo_trips`
--
ALTER TABLE `cargo_trips`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `collections`
--
ALTER TABLE `collections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `demurrages`
--
ALTER TABLE `demurrages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expense_heads`
--
ALTER TABLE `expense_heads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `income_heads`
--
ALTER TABLE `income_heads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `managers`
--
ALTER TABLE `managers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_permissions`
--
ALTER TABLE `role_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ships`
--
ALTER TABLE `ships`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ship_expenses`
--
ALTER TABLE `ship_expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ship_expense_heads`
--
ALTER TABLE `ship_expense_heads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trips`
--
ALTER TABLE `trips`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agencies`
--
ALTER TABLE `agencies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `basic_settings`
--
ALTER TABLE `basic_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cargos`
--
ALTER TABLE `cargos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cargo_collections`
--
ALTER TABLE `cargo_collections`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cargo_expenses`
--
ALTER TABLE `cargo_expenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `cargo_expense_heads`
--
ALTER TABLE `cargo_expense_heads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cargo_trips`
--
ALTER TABLE `cargo_trips`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `collections`
--
ALTER TABLE `collections`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `demurrages`
--
ALTER TABLE `demurrages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `expenses`
--
ALTER TABLE `expenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `expense_heads`
--
ALTER TABLE `expense_heads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `income_heads`
--
ALTER TABLE `income_heads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `managers`
--
ALTER TABLE `managers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `role_permissions`
--
ALTER TABLE `role_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ships`
--
ALTER TABLE `ships`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `ship_expenses`
--
ALTER TABLE `ship_expenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `ship_expense_heads`
--
ALTER TABLE `ship_expense_heads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `trips`
--
ALTER TABLE `trips`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
