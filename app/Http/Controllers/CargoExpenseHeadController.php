<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CargoExpenseHead;

class CargoExpenseHeadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['cargo_expense_heads'] = CargoExpenseHead::where('status', '!=', -1)->get();
        return view('cargo_expense_head/index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cargo_expense_head/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        CargoExpenseHead::create($request->all());
        return redirect()->route('cargo_expense_heads.index')->withStatus( __('Cargo Expense Head Successfully Insert!') ); 

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['cargo_expense_head'] = CargoExpenseHead::findorFail($id);
        return view('cargo_expense_head/edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        CargoExpenseHead::where('id',$id)->update($request->only(['name','status']));
        return redirect()->route('cargo_expense_heads.index')->withStatus(__("Cargo expense head information updated successfully."));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cargo_expense_head = CargoExpenseHead::find($id);
        $cargo_expense_head->delete();

        return redirect()->route('cargo_expense_heads.index')->withStatus( __('Cargo expense head successfully delete') );
    }
}
