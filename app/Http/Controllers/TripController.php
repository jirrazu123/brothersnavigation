<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Trip;
use App\Models\Ship;
use Carbon\Carbon;


class TripController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['trips'] = Trip::where('status',1)->orderBy('id','desc')->paginate(10);
        return view('trip.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $date = Carbon::today();
        $t_id = count(Trip::all()) + 1;
        $data['trip_id'] = 'SOFTID-'.$date->year.$date->month.$date->day. '-'.str_pad($t_id,4,'0',STR_PAD_LEFT);
        $data['ships'] = Ship::where('status','!=',-1)->get();
        return view('trip.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        // return ;
        Trip::create($request->all());
        return redirect()->route('trips.index', compact('trips'))->withStatus(__('User successfully created.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['trip'] = Trip::findorFail($id);
        $data['ships'] = Ship::where( 'status', '!=', -1)->get();
        return view('trip.edit', $data); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Trip::where('id',$id)->update($request->only(['name','ship_id','start_date','end_date','status',]));
        return redirect()->route('trips.index',array('id'=>$id))->withStatus(__("Ship information updated successfully."));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Trip::where('id', $id)->delete();
         return redirect()->route('trips.index')->withStatus(__('Trip data deleted successfully!'));
    }
}
