<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customer;
use App\Models\Cargo;
use App\Models\Agency;
use App\Models\Product;
use App\Models\CargoTrip;
use App\Models\Demurrage;
use Carbon\Carbon;

class CargoTripController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cargo_trips = CargoTrip::all();
        return view('cargo_trip/index',compact('cargo_trips'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['customers'] = Customer::all();
        $data['damarages'] = Demurrage::all();
        $data['cargos'] = Cargo::where('status','!=',-1)->get();
        $data['agencys'] = Agency::all();
        $data['products'] = Product::where('status','!=',-1)->get();
        $date = Carbon::today();
        $t_id = count(CargoTrip::all()) + 1;
        $data['trip_id'] = 'SOFTID-'.$date->year.$date->month.$date->day. '-'.str_pad($t_id,4,'0',STR_PAD_LEFT);
       
        return view('cargo_trip.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        // return;
        CargoTrip::create($request->all());
        return redirect()->route('cargo_trips.index')->withStatus(__('Cargo information created successfully.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['cargo_trip'] = CargoTrip::findorFail($id);
        $data['damarages'] = Demurrage::all();
        $data['customers'] = Customer::all();
        $data['cargos'] = Cargo::where('status','!=',-1)->get();
        $data['agencys'] = Agency::all();
        $data['products'] = Product::where('status','!=',-1)->get();
        return view('cargo_trip.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        CargoTrip::where('id',$id)->update($request->only(['customer_id', 'cargo_id', 'agency_id', 'product_id', 'weight', 'rate', 'des_form', 'des_to', 'selling_date', 'unload_date','is_damarage', 'status','demurrage_id', 'demurrage_amount', 'demurrage_date', 'demurrage_received_date', 'damarage_status']));
        return redirect()->route('cargo_trips.index',array('id'=>$id))->withStatus(__("Cargo Trip information updated successfully."));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cargo_trip = CargoTrip::findOrFail($id);
		try{
            CargoTrip::findOrFail($id)->delete();
        }catch (\Exception $e){
            
            return redirect()->route('cargo_trips.index', $cargo_trip->id)->withStatus(__('customer information can not delete. Data exist is another record.'));
        }

        
		return redirect()->route('cargo_trips.index', $cargo_trip->id)->withStatus('success', 'customer information deleted successfully');;
    }
}
