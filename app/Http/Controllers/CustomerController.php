<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Session;

class CustomerController extends Controller {

	public function index() {
		$menu = ['sales', 'customer'];
		$customers = Customer::orderBy('id', 'desc')->get();
		return view('customers.index', compact('menu', 'customers'));
	}

	public function create() {

		$menu = ['sales', 'customer'];
		return view('customers.create', compact('menu'));
	}

	public function store(Request $request) {

		$this->validate($request, [
			'name' => 'required|min:3'
		]);

		$customer = new Customer();
		$customer->name = $request->name;
		$customer->address = $request->address;
		$customer->phone = $request->phone;
		$customer->mobile = $request->mobile;
		$customer->email = $request->email;
		$customer->contact_name = $request->contact_name;
		$customer->contact_designation = $request->contact_designation;
		$customer->contact_phone = $request->contact_phone;
		$customer->contact_mobile = $request->contact_mobile;
		$customer->contact_email = $request->contact_email;
		$customer->invoice_Address = $request->invoice_Address;
		$customer->save();
		Session::flash('message', 'Customer Save Successfully');
		return redirect()->route('customers.index', $customer->id)->with('success', 'Customer created successfully');
	}

	public function show($id) {
		//
	}

	public function edit($id) {
		$customer = Customer::find($id);
		$menu = ['sales', 'customer'];
		return view('customers.edit', compact('customer', 'menu'));
	}

	public function update(Request $request, $id) {

		$this->validate($request, [
			'name' => 'required|min:3'
		]);
		$customer = Customer::find($id);
		$customer->name = $request->input('name');
		$customer->address = $request->input('address');
		$customer->phone = $request->input('phone');
		$customer->mobile = $request->input('mobile');
		$customer->email = $request->input('email');
		$customer->contact_name = $request->input('contact_name');
		$customer->contact_designation = $request->input('contact_designation');
		$customer->contact_phone = $request->input('contact_phone');
		$customer->contact_mobile = $request->input('contact_mobile');
		$customer->contact_email = $request->input('contact_email');
		$customer->invoice_Address = $request->input('invoice_Address');
		$customer->save();
		Session::flash('update', 'Customer has been updated');
		return redirect()->route('customers.index')
			->with('success', 'Customer updated successfully');
	}

	public function destroy($id) {
		$customer = Customer::findOrFail($id);
		try{
            Customer::findOrFail($id)->delete();
        }catch (\Exception $e){
            Session::flash('success', 'customer information can not delete. Data exist is another record.');
            return redirect()->route('customers.index', $customer->id);
        }

        Session::flash('success', 'customer information deleted successfully');
		return redirect()->route('customers.index', $customer->id);
	}
}
