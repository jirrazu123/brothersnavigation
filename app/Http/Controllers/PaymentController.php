<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CargoTrip;
use App\Models\Payment;
use App\Models\Customer;
use App\Models\Cargo;
use App\Models\Agency;
use App\Models\Product;
use App\Models\Demurrage;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $payments = Payment::all();
        return view('payment/index', compact('payments'));
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['agencys'] = Agency::all();
        $data['cargos'] = Cargo::where('status','!=',-1)->get();
        return view('payment/create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        Payment::create($request->all());
        return back()->withStatus(__('Payment Create Successfully.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $data['payment'] = Payment::findorfail($id);
       $data['agencys'] = Agency::all();
       return view('payment.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // return $request->all();
        Payment::where('id', $id)->update($request->only(['agency_id', 'current_balance', 'payment_amount', 'payment_date', 'payment_method', 'remark', 'status',]));
        return redirect()->route('payments.index')->withStatus('Update payment data successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $payment = Payment::find($id);
        $payment->delete();
        return redirect()->route('payments.index')->withstatus('Delete payment data successfully.');
    }
}
