<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Demurrage;

class DemurrageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['demurrages'] = Demurrage::where('status', '!=', -1)->get();
        return view('demurrage/index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('demurrage/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Demurrage::create($request->all());
        return redirect()->route('demurrages.index')->withStatus( __('Demurrage Information  Successfully Insert!') ); 

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['demurrage'] = Demurrage::findorFail($id);
        return view('demurrage/edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Demurrage::where('id',$id)->update($request->only(['demurrage_type','sort_des','status']));
        return redirect()->route('demurrages.index')->withStatus(__("Demurrage information updated successfully."));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $demurrage = Demurrage::find($id);
        $demurrage->delete();

        return redirect()->route('demurrages.index')->withStatus( __('Demurrage information  successfully delete.') );
    }
}
