<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Trip;
use App\Models\IncomeHead;
use App\Models\Collection;

class CollectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $data['trips'] = Trip::where('is_collection',1)->orderBy('id','desc')->paginate(10);
         return view('collection/index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id=null)
    {

        $data['trips'] =  Trip::where('status', '!=', -1)->get();

        $data['trips'] =  Trip::where('is_collection', '!=', 1)->get();
        $data['income_heads'] =  IncomeHead::where('status',1)->get();
        $data['trip_id'] = @$_GET['id'];
        return view('collection/create', $data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         
            $post_data = $request->input();
            $total_income_up = 0;
            foreach( $post_data['head_id_up'] as $key=>$value){
                $data_up = array(
                    'trip_id'=>$post_data['trip_id'],
                    'income_head_id'=>$value, 
                    'amount'=>$post_data['amount_up'][$key], 
                    'status'=>1,
                    'type_id'=>1
                );
                Collection::create($data_up); 
                $total_income_up +=$post_data['amount_up'][$key];
            }
            Trip::where('id',$post_data['trip_id'])->update(array('is_collection' =>1,'total_up_collection'=>$total_income_up));
            #Down 
            $total_income_down = 0;
            foreach( $post_data['head_id_down'] as $key_down=>$value_down){
                $data_down = array(
                    'trip_id'=>$post_data['trip_id'],
                    'income_head_id'=>$value_down, 
                    'amount'=>$post_data['amount_down'][$key_down], 
                    'status'=>1,
                    'type_id'=>2
                );
                Collection::create($data_down); 
                $total_income_down +=$post_data['amount_down'][$key_down];
            }
            Trip::where('id',$post_data['trip_id'])->update(array('is_collection' =>1,'total_down_collection'=>$total_income_down));
            

            return redirect()->route('collections.index')->withStatus(__('Collections data  successfully Added!'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $trip = Trip::findOrFail($id);
        //return $trip;//->ship->expenses->expenseHead;
        // return $trip->expenses->expenseHead;
        return view('collection/show')->with([
            'trip'  => $trip
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['trips'] =  Trip::where('status', 1)->get();
        $data['income_heads'] =  IncomeHead::where('status',1)->get();
        $data['collections'] = Collection::where('trip_id',$id)->get();
        $data['trip_id'] = $id; 
        return view('collection.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $post_data = $request->input();
         Collection::where('trip_id',$post_data['trip_id'])->delete();

            $total_income_up = 0;
            foreach( $post_data['head_id_up'] as $key=>$value){
                $data_up = array(
                    'trip_id'=>$post_data['trip_id'],
                    'income_head_id'=>$value, 
                    'amount'=>$post_data['amount_up'][$key], 
                    'status'=>1,
                    'type_id'=>1
                );
                Collection::create($data_up); 
                $total_income_up +=$post_data['amount_up'][$key];
            }
            Trip::where('id',$post_data['trip_id'])->update(array('is_collection' =>1,'total_up_collection'=>$total_income_up));
            #Down 
            $total_income_down = 0;
            foreach( $post_data['head_id_down'] as $key_down=>$value_down){
                $data_down = array(
                    'trip_id'=>$post_data['trip_id'],
                    'income_head_id'=>$value_down, 
                    'amount'=>$post_data['amount_down'][$key_down], 
                    'status'=>1,
                    'type_id'=>2
                );
                Collection::create($data_down); 
                $total_income_down +=$post_data['amount_down'][$key_down];
            }
            Trip::where('id',$post_data['trip_id'])->update(array('is_collection' =>1,'total_down_collection'=>$total_income_down));
            

            return redirect()->route('collections.index')->withStatus(__('Collections data  successfully Added!'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Collection::where('trip_id', $id)->delete();
         return redirect()->route('collections.index')->withStatus(__('Collection data deleted successfully.'));
    }
}
