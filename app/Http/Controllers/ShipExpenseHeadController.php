<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ShipExpenseHead;

class ShipExpenseHeadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ship_expense_heads = ShipExpenseHead::all();
        return view('ship_expense_head/index', compact('ship_expense_heads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ship_expense_head/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        ShipExpenseHead::create($request->all());
       return redirect()->route('ship_expense_heads.index')->withStatus(__('Expense Head successfuly created.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['ship_expense_head'] = ShipExpenseHead::findorFail($id);
        return view('ship_expense_head/edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        ShipExpenseHead::where('id', $id)->update($request->only('name', 'status'));
        return redirect()->route('ship_expense_heads.index')->withStatus(__("Ship Expense head updated successfully."));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ship_expense_head = ShipExpenseHead::find($id);
        $ship_expense_head->delete();
   
        return redirect()->route('ship_expense_heads.index')->withStatus(__('Ship expense head has been deleted Successfully') );
    }
}
