<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employee;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('employee/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('employee/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return $request->all();
        $request->validate([
            'name'    => 'required',
            'mobile'  => 'required',
            'address' => 'required',
            'nid'     => 'required',
            'joining_date' => 'required',
            'department'   => 'required'
        ]);

        //  $request->file('image')->getClientOriginalExtension()->move(public_path('images'));
        // // $new_name = rand() . '.' . $image->getClientOriginalExtension();
        //  //$image->move(public_path('images'), $image);

        // Employee::create($request->all());
        // back();

        // $image = $request->file('image');
        // $new_name = rand().'.'.$iamge->getClientOriginalExtension();
        // $image->move(public_path('image'), $image);
        
        $image = $request->file('image');

        $new_name = rand() . '.' . $image->getClientOriginalExtension();
        $image->move(public_path('images'), $new_name);

        $fill_data = array (
            'name'         => $request->name,
            'mobile'       => $request->mobile,
            'address'      => $request->address,
            'nid'          => $request->nid,
            'joining_date' => $request->joinig_date,
            'department'   => $request->department,
            'job_type'     => $request->job_type,
            'salary'       => $request->salary,
            'remark'       => $request->remark,
            'status'       => $request->status,
            'image'        => $new_name
        );
        Employee::create($fill_data);

        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
