<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ShipExpense;
use App\Models\Ship;
use App\Models\ShipExpenseHead;

class ShipExpenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['ships'] = Ship::with(['expenses.shipExpenseHead'])->where('status',1)->get();
        $data['expenses'] = ShipExpense::with(['ship', 'shipExpenseHead'])->where('status',1)->paginate(10);

        //return $data;
        return view('ship_expense/index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['ship_expense_heads'] = ShipExpenseHead::where('status', '!=', -1)->get();
        $data['ships'] = Ship::where('status', '!=', -1)->get();
        return view('ship_expense/create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        ShipExpense::create($request->all());
        return redirect()->route('ship_expenses.index')->withStatus(__('Expense has been created Successfully') );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['expense'] = ShipExpense::findorFail($id);
        $data['ships'] = Ship::where('status', '!=', -1)->get();
        $data['ship_expense_heads'] = ShipExpenseHead::where('status', '!=', -1)->get();
        return view('ship_expense.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        ShipExpense::where('id', $id)->update($request->only(['head_id', 'amount', 'status','ship_id','expense_date']));
        return redirect()->route('ship_expenses.index')->withStatus(__("Ship Expense information updated successfully."));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $expense = ShipExpense::find($id);
       $expense->delete();
        return redirect()->route('ship_expenses.index')->withStatus(__('Ship expenses has been deleted Successfully') );
    }
}
