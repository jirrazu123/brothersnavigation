<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CargoTrip;
use App\Models\CargoExpense;
use App\Models\CargoExpenseHead;
use App\Models\Cargo;

class CargoExpenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['cargo_trips'] = CargoTrip::where('is_expense',1)->get();
        // $data['cargo_expenses'] = CargoExpense::all();
        return view('cargo_expense/index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$data['cargos'] = Cargo::all();
        $data['cargo_expense_heads'] = CargoExpenseHead :: all();
        $data['cargo_trips'] = CargoTrip::all();
        $data['cargo_trip_id'] = @$_GET['id'];
        return view('cargo_expense/create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //  dd($request->all());
        //  return; 
        $post_data = $request->input();
        $total_expence = 0;
        foreach($post_data['head_id'] as $key=>$value){
            $data = array(
                'cargo_trip_id' =>$post_data['cargo_trip_id'],
                'head_id' => $value,
                'amount'=>$post_data['amount'][$key], 
                'status'=>1
            );
            // dd($data);
            // return;
            CargoExpense::create($data);
            $total_expence +=$post_data['amount'][$key];
            
        }
        CargoTrip::where('id',$post_data['cargo_trip_id'])->update(array('is_expense' =>1,'total_expense'=>$total_expence));
        return redirect()->route('cargo_expenses.index')->withStatus(__('Cargo Expense data  successfully created.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $data['cargo_trips'] =  CargoTrip::where('status', 1)->get();
        $data['cargo_expense_heads'] =  CargoExpenseHead::where('status',1)->get();
        $data['cargo_expenses'] = CargoExpense::where('cargo_trip_id',$id)->get();
        $data['cargo_trip_id'] = $id; 
       
        return view('cargo_expense.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        CargoExpense::where('cargo_trip_id', $request->input('cargo_trip_id'))->delete(); 
        $post_data = $request->input();
        $total_expence = 0;
        foreach($post_data['head_id'] as $key=>$value){
            $data = array(
                'cargo_trip_id' =>$post_data['cargo_trip_id'],
                'head_id' => $value,
                'amount'=>$post_data['amount'][$key], 
                'status'=>1
            );
            // dd($data);
            // return;
            CargoExpense::create($data);
            $total_expence +=$post_data['amount'][$key];
            
        }
        CargoTrip::where('id',$post_data['cargo_trip_id'])->update(array('is_expense' =>1,'total_expense'=>$total_expence));
        return redirect()->route('cargo_expenses.index')->withStatus(__('Update expenses data successfully.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CargoExpense::where('cargo_trip_id', $id)->delete();
         return redirect()->route('cargo_expenses.index')->withStatus(__('Expenses data deleted successfully.'));
    }
}
