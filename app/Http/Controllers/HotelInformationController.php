<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\HotelInformation;

class HotelInformationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hotelinformations = HotelInformation::all();
        return view('hotel_information/index', compact('hotelinformations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('hotel_information/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        //imge upload check
        
        if($request->hasFile('image')){
            //store image
            $request->image->store('public/images');
        }
        //store all data 
        HotelInformation::create($request->all());
        return redirect()->route('hotel_informations.index', compact('hotelinformation'))->withStatus(__('Room Information Create Successfully.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['hotelinformation'] = HotelInformation::findorFail($id);
        return view('hotel_information.edit', $data); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        // return;

        HotelInformation::where('id', $id)->update($request->only(['room_type', 'room_number', 'image', 'fiture', 'capacity', 'remark', 'status',]));
        return redirect()->route('hotel_informations.index')->withStatus('Update room data successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hotelinformation = HotelInformation::find($id);
        $hotelinformation->delete();
        return redirect()->route('hotel_informations.index')->withstatus('Delete room data successfully.');
    }
}
