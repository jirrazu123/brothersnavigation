<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\HotelExpense;

class HotelExpenseController extends Controller
{
/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        $data['hotel_expenses'] = HotelExpense::where('is_expense',1)->orderBy('id','desc')->paginate(10);
        return view('hotel_expense/index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id = null)
    {
        
        $data['hotel_expense_head'] =  HotelExpense::where('status',1)->get();
        return view('hotel_expense/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//       dd($request->all());
//       return;
            $post_data = $request->input();
            $total_expence = 0;
            foreach( $post_data['head_id'] as $key=>$value){
                $data = array(
                    'trip_id'=>$post_data['trip_id'],
                    'head_id'=>$value,
                    'remark' =>$post_data['remark'][$key],
                    'amount'=>$post_data['amount'][$key], 
                    'status'=>1
                );
                Expense::create($data);
                $total_expence +=$post_data['amount'][$key];
            }
            Trip::where('id',$post_data['trip_id'])->update(array('is_expense' =>1,'total_expense'=>$total_expence));
             return redirect()->route('expenses.index', compact('expenses'))->withStatus(__('Expense data  successfully created.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['trips'] =  Trip::where('status', 1)->get();
        $data['expense_heads'] =  ExpenseHead::where('status',1)->get();
        $data['expenses'] = Expense::where('trip_id',$id)->get();
        $data['trip_id'] = $id; 
        return view('expense.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Expense::where('trip_id', $request->input('trip_id'))->delete();  
            $post_data = $request->input();
            $total_expence =0;
            foreach( $post_data['head_id'] as $key=>$value){
                $data = array(
                    'trip_id'=>$post_data['trip_id'],
                    'head_id'=>$value, 
                    'amount'=>$post_data['amount'][$key],
                    'remark'=>$post_data['remark'][$key],
                    'status'=>1
                );
                Expense::create($data);
                $total_expence +=$post_data['amount'][$key];
            }
            Trip::where('id',$post_data['trip_id'])->update(array('is_expense' =>1,'total_expense'=>$total_expence));
        return redirect()->route('expenses.index')->withStatus(__('Update expenses data successfully.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         Expense::where('trip_id', $id)->delete();
         return redirect()->route('expenses.index')->withStatus(__('Expenses data deleted successfully.'));
    }
}
