<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    protected $fillable = [
        'trip_id','head_id','amount','remark','status',
    ];

    public function trip()
    {
        return $this->belongsTo('App\Models\Trip', 'trip_id', 'id');
    }


    public function ship()
    {
        return $this->belongsTo('App\Models\Ship', 'ship_id', 'id');
    }



    public function expenseHead()

    {
        return $this->belongsTo('App\Models\ExpenseHead', 'head_id', 'id');
    }
    

    
}
