<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    protected $fillable = [
        'name',
        'trip_auto_id',
        'ship_id',
        'start_date',
        'end_date', 
        'status',      
    ];

    public function ship()
    {
        return $this->belongsTo('App\Models\Ship', 'ship_id', 'id');
    }
    public function expenses()
    {
        return $this->hasMany('App\Models\Expense', 'trip_id', 'id');
    }

    public function collections()
    {
        return $this->hasMany('App\Models\Collection', 'trip_id', 'id');
    }
    public function upCollections(){
        return $this->belongsTo('App\Models\Collerction', 'trip_id', 'id')->where('trip_id' == 1);
    }
}
