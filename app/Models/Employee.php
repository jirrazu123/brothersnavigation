<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = [
        'name', 'image', 'mobile', 'address', 'nid', 'job_type', 'department', 'salary', 'remark', 'joining_date', 'status'
    ];
}
