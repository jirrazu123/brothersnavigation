<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HotelExpenseHead extends Model
{
    protected $fillable = [
        'name', 'remark', 'status',
    ];
}
