<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cargo extends Model
{
    protected $fillable = [
        'name', 'capacity', 'status'
    ];

    public function cargoExpenses(){
        return $this->hasMany('App\Models\cargoExpense','cargo_id','id');
    }

    public function cargoTrip(){
        return $this->hasMany('App\Models\cargoExpense','cargo_id','id');
    }
}
