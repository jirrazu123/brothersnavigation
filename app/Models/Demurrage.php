<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Demurrage extends Model
{
    protected $fillable = [
        'demurrage_type','sort_des', 'status',
    ];
}
