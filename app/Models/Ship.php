<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ship extends Model
{
    protected $fillable = [
        'name', 'deluxe', 'cabin','dak', 'status',
    ];

    public function expenses(){
        return $this->hasMany('App\Models\ShipExpense','ship_id','id');
    }

    public function trip(){
        return $this->hasMany('App\Models\ShipExpense','ship_id','id');
    }
}
