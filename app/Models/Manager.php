<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Manager extends Model
{
    protected $fillable = [
        'name', 'nid', 'mobile', 'photo', 'present_address', 'permanent_address', 'status',
    ];
    
}
