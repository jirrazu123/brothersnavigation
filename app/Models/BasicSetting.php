<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BasicSetting extends Model
{
    protected $fillable = [
        'company_name','logo','address','contact','status',
    ];
    
}
