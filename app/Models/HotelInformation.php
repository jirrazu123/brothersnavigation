<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HotelInformation extends Model
{
    protected $fillable = [
        'room_type',
        'room_number',
        'image',
        'fiture',
        'capacity',
        'status',
        'remark'
    ];
}
