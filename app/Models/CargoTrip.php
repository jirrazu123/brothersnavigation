<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CargoTrip extends Model
{
    protected $fillable = [
        'trip_auto_id', 'customer_id', 'demurrage_id', 'demurrage_amount', 'demurrage_date', 'demurrage_received_date', 'damarage_status', 'cargo_id', 'agency_id', 'product_id', 'weight', 'rate', 'des_form', 'des_to', 'selling_date', 'unload_date','is_damarage', 'status',
    ];

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'customer_id', 'id');
    }

    public function cargo() 
    {
        return $this->belongsTo('App\Models\Cargo', 'cargo_id', 'id');
    } 

    public function damurrage() 
    {
        return $this->belongsTo('App\Models\Demurrage', 'demurrage_id', 'id');
    }  

    public function agency()
    {
        return $this->belongsTo('App\Models\Agency', 'agency_id', 'id');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id', 'id');
    }
    public function expenses()
    {
        return $this->hasMany('App\Models\CargoExpense', 'cargo_trip_id', 'id');
    }
}
