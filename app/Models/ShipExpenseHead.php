<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShipExpenseHead extends Model
{
    protected $fillable = [
        'name', 'status',
    ];
}
