<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CargoExpenseHead extends Model
{
    protected $fillable = [
        'name', 'status',
    ];
}
