<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ShipExpense extends Model
{
    public $timestamps = true;
    protected $fillable = [
        'head_id', 'amount', 'status','ship_id','expense_date'
    ];

    public function trip()
    {
        return $this->belongsTo('App\Models\Trip', 'trip_id', 'id');
    }

    public function ship()
    {
        return $this->belongsTo('App\Models\Ship', 'ship_id', 'id');
    }
    
    public function shipExpenseHead()
    {
        return $this->belongsTo('App\Models\ShipExpenseHead', 'head_id', 'id');
    }

    public function setExpenseDateAttribute($value)
    {
        $this->attributes['expense_date'] = Carbon::parse($value)->format('Y-m-d');
    }
}
