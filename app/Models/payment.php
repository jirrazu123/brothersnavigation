<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class payment extends Model
{
    protected  $fillable = [
      
        'agency_id',
        'current_balance',
        'payment_amount',
        'payment_date',
        'payment_method',
        'remark',
        'status'
    ];

    public function agency(){
        return $this->belongsTo('App\Models\Agency', 'agency_id', 'id');
    }

     
}
