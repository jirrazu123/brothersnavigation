<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Collection extends Model
{
    protected $fillable = [
        'type_id','trip_id','income_head_id','amount','status',
    ];
    
    public function trip()
    {
        return $this->belongsTo('App\Models\Trip', 'trip_id', 'id');
    }
    public function income_head()
    {
        return $this->belongsTo('App\Models\IncomeHead', 'income_head_id', 'id');
    }
}
