<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agency extends Model
{
    protected $fillable = [
        'name', 'address', 'email', 'phone', 'contact_person', 'contact_no', 'current_balance',
    ];
}
