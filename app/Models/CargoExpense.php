<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CargoExpense extends Model
{
    protected $fillable = [
        'cargo_trip_id', 'head_id', 'amount', 'status'
    ];

    public function cargo_trip()
    {
        return $this->belongsTo('App\Models\CargoTrip', 'cargo_trip_id', 'id');
    }

    public function expenseHead() 

    {
        return $this->belongsTo('App\Models\CargoExpenseHead', 'head_id', 'id');
    }


}
