<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\CargoTrip;
use App\Observers\CargoObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        CargoTrip::observe(CargoObserver::class);
    }
}
