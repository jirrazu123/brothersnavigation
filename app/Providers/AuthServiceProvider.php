<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Models\Permission;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        foreach($this->getPermissions() as $permission){
            Gate::define($permission->permission_name, function ($user) use($permission) {
                if($user->role){
                    if($user->role->permissions){
                        return $user->role->permissions->contains($permission->id) ? true : false;
                    }
                }
                return false;
            });
        }
        
    }
    private function getPermissions(){
        $permissions = Permission::all();
        return $permissions;
    }
}
