<?php

namespace App\Observers;

use App\Models\CargoTrip;

class CargoObserver
{
    /**
     * Handle the cargo trip "created" event.
     *
     * @param  \App\Models\CargoTrip  $cargoTrip
     * @return void
     */
    public function created(CargoTrip $cargoTrip)
    {
        //
    }

    public function creating(CargoTrip $cargoTrip)
    {
        // dd($cargoTrip->getOriginal());
    }

    public function updating(CargoTrip $cargoTrip)
    {
        // dd($cargoTrip);
        // dd($cargoTrip->getOriginal());
    }

    /**
     * Handle the cargo trip "updated" event.
     *
     * @param  \App\Models\CargoTrip  $cargoTrip
     * @return void
     */
    public function updated(CargoTrip $cargoTrip)
    {
        //
    }

    /**
     * Handle the cargo trip "deleted" event.
     *
     * @param  \App\Models\CargoTrip  $cargoTrip
     * @return void
     */
    public function deleted(CargoTrip $cargoTrip)
    {
        //
    }

    /**
     * Handle the cargo trip "restored" event.
     *
     * @param  \App\Models\CargoTrip  $cargoTrip
     * @return void
     */
    public function restored(CargoTrip $cargoTrip)
    {
        //
    }

    /**
     * Handle the cargo trip "force deleted" event.
     *
     * @param  \App\Models\CargoTrip  $cargoTrip
     * @return void
     */
    public function forceDeleted(CargoTrip $cargoTrip)
    {
        //
    }
}
