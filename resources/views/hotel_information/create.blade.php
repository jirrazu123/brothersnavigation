@extends('layouts.app', ['title' => __('Create Room')])

@section('content')
    @include('users.partials.header', ['title' => __('Create Room Information')])   

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Create Room information') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('hotel_informations.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('hotel_informations.store') }}" autocomplete="off">
                            @csrf
                            
                            
                            <div class="pl-lg-4">
                            <div class="form-row">

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="room_type">{{ __('Room Type') }}</label>
                                    <input type="text" name="room_type" id="name" class="form-control form-control-alternative" placeholder="{{ __('Room Type') }}" required>
                                </div><!--//Room Type-->

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="room_number">{{ __('Room Number') }}</label>
                                    <input type="text" name="room_number" id="room_number" class="form-control form-control-alternative" placeholder="{{ __('Room Number') }}" required>

                                </div>

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="input-name">{{ __('Images') }}</label>
                                    <input type="file" name="image" class="form-control form-control-alternative" placeholder="{{ __('Upload photo') }}" >
                                </div>

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="capacity">{{ __('Capacity') }}</label>
                                    <input type="text" name="capacity" id="capacity" class="form-control form-control-alternative" placeholder="{{ __('Capacity') }}" required>

                                </div>

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="fiture">{{ __('Room Features') }}</label>
                                    <textarea rows="4" cols="50" name="fiture" id="fiture" class="form-control form-control-alternative" placeholder="{{ __('Room Features') }}" required autofocus ></textarea>

                                </div>

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="remark">{{ __('Remark') }}</label>
                                    <input type="text" name="remark" id="remark" class="form-control form-control-alternative" placeholder="{{ __('Write remarks') }}" required>

                                </div>

                                <div class="form-group col-md-12">
                                    <h3>Status</h3>
                                    @php
                                        if(@$hotelinformation->status){
                                        $active = 'checked';
                                        $inactive = '';
                                        }else{
                                        $active = '';
                                        $inactive = 'checked';
                                    }
                                    @endphp
                                    
                                    <div class="custom-control custom-radio mb-3">
                                    <input name="status" class="custom-control-input" id="status1" type="radio" value="0" {{ $inactive }}>
                                    <label class="custom-control-label" for="status1">inactive</label>
                                    </div>
                                    <div class="custom-control custom-radio mb-3">
                                    <input name="status" class="custom-control-input" value="1" id="status2"  type="radio"  {{ $active.(!Request::segment(3)) ? 'checked' : ''}}>
                                    <label class="custom-control-label" for="status2">active</label>
                                    </div>
                                </div>
                                
                            </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        @include('layouts.footers.auth')
    </div>
@endsection