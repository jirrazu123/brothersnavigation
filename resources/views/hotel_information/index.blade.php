@extends('layouts.app', ['title' => __('Hotel Information')])

@section('content')
    @include('layouts.headers.cards')

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Hotel Room Information') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('managers.create') }}" class="btn btn-sm btn-primary">{{ __('Add Room') }}</a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-12">
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                    </div>

                    <div class="table-responsive p-4">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th>{{ __('#SL') }}</th>
                                    <th>{{ __('Hotel Room Information') }}</th>
                                    <th>{{ __('Image') }}</th>
                                    <th>{{ __('Features') }}</th>
                                    <th>{{ __('Status') }}</th>
                                    <th>{{__('Action') }}</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            @php
                                @$status=['unbooked','booked'];
                            @endphp
                               @foreach($hotelinformations as $key=>$hotelinformation)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>Type of Room: {{  $hotelinformation->room_type }} </br>
                                       Room number: {{ $hotelinformation->room_number }} 
                                       </br>
                                       Capacity : {{ $hotelinformation->capacity }} 
                                    </td>
                                    <td>
                                    <img src="{{ Storage::url($hotelinformation->image) }}" alt="" width="100px">
                                    </td>
                                    <td><span><strong>Remark:</strong> {{ $hotelinformation->remark }}</span>
                                    </td>
                                    <td>{{ $status[$hotelinformation->status] }}</td>
                                    
                                    <td>
                                        <div style="display:inlineblock;">
                                            <a href="{{ route('hotel_informations.edit', array('id' => $hotelinformation->id))}}" class="btn btn-sm btn-info">Edit</a>
                                    </td>
                                    <td>       
                                            <form action="{{ route('hotel_informations.destroy',  array('id'=>$hotelinformation->id)) }}" method="POST">
                                            @csrf 
                                            <input type="hidden" name="_method" value="DELETE">
                                            <button class="btn btn-sm btn-danger" onclick="return deleteFunction()" type="submit">Delete</button>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                               @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            
                        </nav>
                    </div>
                </div>
            </div>
        </div>
            
        @include('layouts.footers.auth')
    </div>
@endsection
@section('script')
<script>
    function deleteFunction(){
        if(!confirm('Are you sure to delete this?'))
        event.preventDefault();
    }
</script>
@endsection