@extends('layouts.app', ['title' => __('Create Trip Collection')])

@section('content')
    @include('users.partials.header', ['title' => __('Create Trip Collection')])   

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Create Trip Collection ') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('collections.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="card-body">
                            <form method="post" action="{{ route('collections.store') }}" autocomplete="off">
                                @csrf
                                
                               
                                <div class="pl-lg-4">

                                <div class="form-group col-md-12">
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Trip select</label>
                                        <select class="form-control" name="trip_id" id="trip_id" required="">
                                        <option value="">----select----</option>
                                            @foreach($trips as $trip)
                                                <option value="{{ $trip->id }}">{{ $trip->name }} | Ship:  | StartDate: {{ date('d:m:Y', strtotime($trip->start_date)) }} | Enddate: {{ date('d:m:Y', strtotime($trip->end_date)) }}  </option>
                                            @endforeach
                                           
                                        </select>
                                    </div>

                                    @if ($errors->has('trip_id'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('trip_id') }}</strong>
                                        </span>
                                    @endif
                                     
                                </div>
                                
                                <div class="row">
                                
                                <!-- two part -->
                                <div class="col-md-6">
                                    <label class="form-control-label" for="head_id">{{ __('Up Collection') }}</label>
                                      <hr>
                                    <div class="form-row">
                                    
                                        <div class="row col-md-10" id="expense_mult">
                                            
                                                <!-- js form add -->
                                                <div class="form-group col-md-5">
                                                    <label class="form-control-label" for="head_id">{{ __(' Income Head') }}</label>
                                                    <select class="form-control" name="head_id_up[]" id="head_id_up[]" required="">
                                                       
                                                            <option value="">---Select---</option>
                                                            @foreach(@$income_heads as $head)
                                                            <option value="{{ @$head->id }}">{{ @$head->name }}</option>

                                                            @endforeach
                                                          
                                                       
                                                    </select>
                                                    
                                                </div>

                                                <div class="form-group col-md-5">
                                                    <label class="form-control-label" for="amount"></label>
                                                    <input type="number" name="amount_up[]" id="amount_up[]" class="form-control form-control-alternative{{ $errors->has('amount') ? ' is-invalid' : '' }}" placeholder="{{ __('Amount') }}" value="" required="">
                                                    
                                                    @if ($errors->has('amount'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('amount') }}</strong>
                                                        </span>
                                                    @endif
                                            
                                                </div>
                                                <!-- js from end -->
                                            
                                        </div>

                                        <div id="new_head" class="row col-md-10" style="display:none"> 
                                        
                                        </div>

                                        <div class="col-md-1 ">
                                            <p class="pt-3"></p>
                                            <span  id="add" class="btn btn-primary px-5" style="width:100%;"><i class="fas fa-plus"></i></span>
                                        </div>
                                    
                                    </div>
                                </div>

                                <!-- down collection -->
                             
                                <div class="col-md-6">
                                    <label class="form-control-label" for="head_id_up[]">{{ __('Down Collection') }}</label>
                                        <hr/>
                                    <div class="form-row">

                                        <div class="row col-md-10" id="expense_mult1">
                                            
                                                <!-- js form add -->
                                                <div class="form-group col-md-5">
                                                    <label class="form-control-label" for="head_id">{{ __('Income Head') }}</label>
                                                    <select class="form-control" name="head_id_down[]" id="head_id" required=""> 
                                                        <option value="">---Select---</option>
                                                       @foreach(@$income_heads as $head)
                                                            <option value="{{ $head->id }}">{{ $head->name }}</option>
                                                        @endforeach
                                                           
                                                            
                                                       
                                                    </select>
                                                    
                                                </div>

                                                <div class="form-group col-md-5">
                                                    <label class="form-control-label" for="amount_down[]"></label>
                                                    <input type="number" name="amount_down[]" id="amount_down[]" class="form-control form-control-alternative{{ $errors->has('amount') ? ' is-invalid' : '' }}" placeholder="{{ __('Amount') }}" value="" required="">
                                                    
                                                    @if ($errors->has('amount'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('amount') }}</strong>
                                                        </span>
                                                    @endif
                                            
                                                </div>
                                                <!-- js from end -->
                                            
                                        </div>

                                        <div id="new_head1" class="row col-md-10" style="display:none"> 
                                        
                                        </div>

                                        <div class="col-md-1 ">
                                            <p class="pt-3"></p>
                                            <span  id="add1" class="btn btn-primary px-5" style="width:100%;"><i class="fas fa-plus"></i></span>
                                        </div>
                                
                                    </div>
                                </div>

                                <!-- ./two part -->
                                </div>
                                
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-success mt-4">{{ __('Save Trip Collection') }}</button>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        @include('layouts.footers.auth')
    </div>
@endsection

@section('script')
<script type="text/javascript">
$(document).ready(function(){

    $(document).on('click', '#add',function(){
      $('#new_head').append(($('#expense_mult').html()));
      $('#new_head').append($('<div class="cross col-md-1 mt-5"><i class="fas fa-times btn btn-sm btn-warning"></i></div>'));
      $('#new_head').css('display',''); 
  })
  $(document).on('click', '.cross', function(){
    $(this).prev().remove();
    $(this).prev().remove();
    $(this).remove();
  }) 


  $(document).on('click', '#add1',function(){
      $('#new_head1').append(($('#expense_mult1').html()));
      $('#new_head1').append($('<div class="cross col-md-1 mt-5"><i class="fas fa-times btn btn-sm btn-warning"></i></div>'));
      $('#new_head1').css('display',''); 
  })
  $(document).on('click', '.cross', function(){
    $(this).prev().remove();
    $(this).prev().remove();
    $(this).remove();
  }) 


});
</script>
@endsection