@extends('layouts.app', ['title' => __('User Management')])

@section('content')
    @include('layouts.headers.cards')

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Trip Collections') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('collections.create') }}" class="btn btn-sm btn-primary">{{ __('Add trip collection') }}</a>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <div class="input-group input-group-alternative my-4">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-zoom-split-in  text-info"></i></span>
                                    </div>
                                    <input class="form-control form-control-alternative" placeholder="Enter Tirp id or name" type="text">
                                    <button class="btn btn-primary">Search</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4"></div>
                        </div>
                    </div>
       
                    <div class="col-12">
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                    </div>

                     <div class="table-responsive">
                        <!-- Collapse -->
                        <div class="container">
                            <div id="accordion">
                           
                               @foreach($trips as $key=>$trip)
                                
                               @if(count($trip->collections) > 0)

                                 <div class="card">
                                    <div class="card-header" id="heading{{ $key }}">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link" data-toggle="collapse" data-target="#collapse{{ $key }}" aria-expanded="true" aria-controls="collapseOne">
                                        Trip Name: <span class="text-success">{{ $trip->name }}</span> | Ship: <span class="text-success">{{ @$trip->ship->name }}</span><span class="px-3 text-warning">| <small> StartDate:{{ date("F j, Y, g:i a", strtotime($trip->start_date)) }} | Enddate: {{ date("F j, Y, g:i a", strtotime($trip->end_date)) }}</small></span>  <span class="float-right badge badge-danger">Total Collection: {{ $trip->total_up_collection + $trip->total_down_collection  }} BDT</span> 
                                         </button>
                                    </h5>
                                    </div>

                                    <div id="collapse{{ $key }}" class="collapse" aria-labelledby="heading{{ $key }}" data-parent="#accordion">
                                    <div class="card-body">
                                    <div class="row">
                                        <div class="table-responsive col-md-6">
                                        <!-- Table start -->
                                        <table class="table">
                                            <thead class="thead-light">
                                                <tr>
                                                   
                                                    <th scope="col">{{ __('Up Collections') }}</th>
                                                    <th scope="col" class="text-right">{{ __('Amount') }}</th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php 
                                                @$grand_total1 = 0;
                                                @endphp
                                                @foreach(@$trip->collections as $collection)
                                                @if($collection->type_id==1)
                                                <tr>

                                                    <td>{{ @$collection->income_head->name }}</td>
                                                    <td class="text-right">{{ $collection->amount }}</td>
                                                </tr>
                                               
                                                @php 
                                                @$grand_total1 += $collection->amount;
                                                @endphp 
                                                @endif
                                                @endforeach
                                                <tr>
                                                    
                                                    <th class="text-primary bg-light">Grand Total</th>
                                                    <th class="text-right text-primary bg-light"> {{ @$grand_total1 }}</th>
                                                </tr>
                                                 
                                                
                                            </tbody>
                                            
                                        </table>
                                            <!-- Table end -->
                                        </div>
                                        <div class="table-responsive col-md-6">
                                        <!-- Table start -->
                                        <table class="table">
                                            <thead class="thead-light">
                                                <tr>
                                                   
                                                    <th scope="col">{{ __('Down Collections') }}</th>
                                                    <th scope="col" class="text-right">{{ __('Amount') }}</th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php 
                                                @$grand_total = 0;
                                                @endphp
                                                @foreach(@$trip->collections as $collection) 
                                                @if($collection->type_id==2)
                                                <tr>

                                                    <td>{{ @$collection->income_head->name }}</td>
                                                    <td class="text-right">{{ $collection->amount }}</td>
                                                </tr>
                                               
                                                @php 
                                                @$grand_total += $collection->amount;
                                                @endphp 
                                                @endif
                                                @endforeach
                                                <tr>
                                                    
                                                    <th class="text-primary bg-light">Grand Total</th>
                                                    <th class="text-right text-primary bg-light"> {{ @$grand_total }}</th>
                                                </tr>
                                                
                                                
                                                
                                            </tbody>
                                            
                                        </table>
                                            <!-- Table end -->
                                            <table class="table">
                                            <tr class="text-right">
                                                    <td colspan="3">
                                                        <div class="dropdown">
                                                            <button class="btn btn-secondary dropdown-toggle bg-primary text-white" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            Option
                                                            </button>
                                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                                <a class="dropdown-item" href="{{ route('collections.show', array('id'=> $trip->id)) }}"><i class="fas fa-edit text-primary"></i>Detail View</a>
                                                                <a class="dropdown-item" href="{{ route('collections.edit', array('id' => $trip->id))}}"><i class="fas fa-edit text-info"></i>Edit</a>
                                                                 <form action="{{ route('collections.destroy',  array('id'=>$trip->id)) }}" method="POST">
                                                                @csrf 
                                                                <input type="hidden" name="_method" value="DELETE">
                                                                <i class="fas fa-trash-alt text-danger pl-3"></i>
                                                                <button class="text-danger bg-white pl-3" onclick="return deleteFunction()" type="submit" style="border:none;">Delete</button>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>    
                                    </div>
                                    </div>
                                </div>
                                @endif
                               @endforeach
                              
                              
                                
                            </div>


                        </div>
                        
                        <!-- Collapse end -->
                    </div>
                        
                        <!-- Collapse end -->
                    </div>
                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                             {{ $trips->links() }}
                        </nav>
                    </div>
                </div>
            </div>
        </div>
            
        @include('layouts.footers.auth')
    </div>
@endsection
@section('script')
<script>
    function deleteFunction(){
        if(!confirm('Are you sure to delete this?'))
        event.preventDefault();
    }
</script>
@endsection