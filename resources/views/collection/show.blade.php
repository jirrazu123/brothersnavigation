@extends('layouts.app', ['title' => __('Show Details')])

@section('content')
    @include('layouts.headers.cards')

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Trip Collections Details') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('collections.index') }}" class="btn btn-sm btn-primary">{{ __('Back to the list') }}</a>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <div class="input-group input-group-alternative my-4">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-zoom-split-in  text-info"></i></span>
                                        </div>
                                        <input class="form-control form-control-alternative" placeholder="Enter Tirp id or name" type="text">
                                        <button class="btn btn-primary">Search</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4"></div>
                        </div>
                    </div>

                    <div class="col-12">
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                    </div>

                    <div class="table-responsive">
                        <!-- Collapse -->
                        <div class="container">
                            <div id="accordion">

                                <div class="card mb-4 border-primary">
                                    <div class="card-header bg-primary">
                                        <h3 class="text-white">Trip Information</h3>
                                    </div>
                                    <div class="card-body">
                                        <table class="table table-bordered">
                                            <thead>

                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td colspan="1">Trip id</td>
                                                <td>{{ $trip->trip_auto_id }}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="1">Ship Name</td>
                                                <td>{{ $trip->ship->name }}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="1">Trip Name</td>
                                                <td>{{ $trip->name }}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="1">Started Date</td>
                                                <td>{{ $trip->start_date }}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="1">End Date</td>
                                                <td>{{ $trip->end_date }}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="1">Total Collected Amount</td>
                                                <td>{{ $trip->total_up_collection+ $trip->total_down_collection }}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                   
                                        <div class="col-md-6">
                                        <div class="card border-primary">
                                            <div class="card-header bg-primary">
                                                <h3 class="text-white">Up collection Information</h3>
                                            </div>
                                            <div class="card-body">
                                                <table class="table table-bordered">
                                                    <thead>
                                                    <td scope="col">Name of Collection Head</td>
                                                    <td scope="col">{{ __("Amount")  }}</td>
                                                    </thead>
                                                    <tbody>
                                                  
                                                        <tr>
                                                            <td scope="col"></td>
                                                            <td scope="col"></td>
                                                        </tr>
                                                   
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                   
                                    <div class="col-md-6">
                                        <div class="card border-primary">
                                            <div class="card-header bg-primary">
                                                <h3 class="text-white">Down collection Information</h3>
                                            </div>
                                            <div class="card-body">
                                                <table class="table table-bordered">
                                                    <thead>

                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td scope="col">Name of Collection Head</td>
                                                        <td scope="col">Total Collection</td>
                                                    </tr>
                                                    <tr>
                                                        <td scope="col"></td>
                                                        <td scope="col"></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                  
                                </div>

                                <div class="card mt-4  border-primary">
                                    <div class="card-header  bg-primary">
                                        <h3 class="text-white">Expense Information</h3>
                                    </div>
                                    <div class="card-body">
                                        <table class="table table-bordered">
                                            <thead>

                                            </thead>
                                            <tbody>

                                           
                                            <tr>
                                                <td scope="col"></td>
                                                <td scope="col"></td>
                                            </tr>
                                            

                                            <tr>
                                                <td scope="col">Total Expense</td>
                                                <td scope="col"></td>
                                            </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="card mt-4  border-primary">
                                    <div class="card-header  bg-primary">
                                        <h3 class="text-white">Remark</h3>
                                    </div>
                                    <div class="card-body">

                                    </div>
                                </div>

                            </div>


                        </div>

                        <!-- Collapse end -->
                    </div>

                    <!-- Collapse end -->
                </div>
                <div class="card-footer py-4">
                    <nav class="d-flex justify-content-end" aria-label="...">

                    </nav>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.footers.auth')
    </div>
@endsection
@section('script')
    <script>
        function deleteFunction(){
            if(!confirm('Are you sure to delete this?'))
                event.preventDefault();
        }
    </script>
@endsection