<div class="row">
    <div class="col-md-8 col-md-offset-2">
       <!--SUCCESS-->
        @if (Session::has('message'))
        <div class="alert alert-success fade in">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
             {!! Session::get('message') !!}
        </div>
        @endif
        <!--DANGER-->
        @if (Session::has('error'))
        <div class="alert alert-danger fade in">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
             {!! Session::get('error') !!}
        </div>
        @endif
    </div>
</div>


<div class="row">
<div class="col-sm-6">
    <div class="form-group">
        <label for="CustomerName" class="control-label"> Customer Name <span class="required" aria-required="true">*</span> </label>
        <input type="text" class="form-control" id="customerName" value="{{ old('name') }}" name="name" placeholder="Customer Name" required>
        @if ($errors->has('name'))
            <label id="email-error" class="error" for="name"> {{ $errors->first('name') }}</label>
        @endif
    </div>
    <div class="form-group">
        <label for="phone-mask"> Phone </label>
        <div class="input-group mb-sm">
            <span class="input-group-addon"><i class="fa fa-phone"></i></span>
            <input type="text" class="form-control" value="{{ old('phone') }}" name="phone" id="phone">

        </div>
    </div>
    <div class="form-group">
        <label for="phone-mask"> Mobile </label>
        <div class="input-group mb-sm">
            <span class="input-group-addon"><i class="fa fa-phone"></i></span>
            {{-- pattern="^(?:\+88|01)?\d{11}$" --}}
            <input type="text" class="form-control" value="{{ old('mobile') }}" name="mobile" id="Mobile" placeholder="EX: +8801721223547">
            @if ($errors->has('mobile'))
                <label id="email-error" class="error" for="mobile"> {{ $errors->first('mobile') }}</label>
            @endif
        </div>
    </div>

</div>

<div class="col-sm-6">
    <div class="form-group">
        <label for="phone-mask"> Email </label>
        <div class="input-group mb-sm">
            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
            <input type="email" class="form-control" value="{{ old('email') }}" name="email" id="Email" placeholder="test@gmail.com">
            @if ($errors->has('email'))
                <label id="email-error" class="error" for="email"> {{ $errors->first('email') }}</label>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="textareaMaxLength" class="control-label"> Customer Address </label>
        <textarea class="form-control" rows="5" value="{{ old('address') }}" name="address" id="Customer Address" placeholder="Write Your Address"></textarea>
        @if ($errors->has('address'))
            <label id="email-error" class="error" for="address"> {{ $errors->first('address') }}</label>
        @endif
    </div>
</div>
</div>

<h4 class="section-subtitle"><b> Contact Person Information </b></h4>
<hr>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label for="CustomerName" class="control-label"> Contact Name <span class="required" aria-required="true">*</span>  </label>
            <input type="text" class="form-control" value="{{ old('contact_name') }}" name="contact_name" id="ContactName" placeholder="Contact Name">
            @if ($errors->has('contact_name'))
                <label id="email-error" class="error" for="contact_name"> {{ $errors->first('contact_name') }}</label>
            @endif
        </div>
        <div class="form-group">
            <label for="Designation" class="control-label"> Designation </label>
            <input type="text" class="form-control" value="{{ old('contact_designation') }}" name="contact_designation" id="Designation" placeholder="Designation">
        </div>
        <div class="form-group">
            <label for="phone-mask"> Phone </label>
            <div class="input-group mb-sm">
                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                <input type="text" class="form-control" value="{{ old('contact_phone') }}" name="contact_phone" id="phone">
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label for="phone-mask"> Mobile  </label>
            <div class="input-group mb-sm">
                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                <input type="text" class="form-control" value="{{ old('contact_mobile') }}" name="contact_mobile" id="Mobile" placeholder="EX: +8801721223547">
                @if ($errors->has('contact_mobile'))
                    <label id="email-error" class="error" for="contact_mobile"> {{ $errors->first('contact_mobile') }}</label>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label for="phone-mask"> Email </label>
            <div class="input-group mb-sm">
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                <input type="email" class="form-control" value="{{ old('contact_email') }}" name="contact_email" id="Email" placeholder="test@gmail.com">
            </div>
        </div>
    </div>
</div>
<h4 class="section-subtitle"><b> Invoice Address </b></h4>
<hr>
<div class="row">
    <div class="col-md-12">
        <!--invoice Address-->
        <div class="form-group">
            <label for="invoiceadd" class="control-label"> Invoice Address </label>
            <textarea class="form-control" rows="3" value="{{ old('invoice_Address') }}" name="invoice_Address" id="invoiceAddress" placeholder="Write Your Address"></textarea>
        </div>

        <div class="form-group text-right">
            <button type="submit" class="btn btn-primary">
                Save Customer Information
            </button>
        </div>

    </div>
</div>
<script>
    $('.alert-success').delay(1000).hide(300).css({'color': 'green'});
    $('.alert-danger').delay(1000).hide(300).css({'color': 'red'});
</script>