@extends('layouts.app', ['title' => __('Ships Entry')])

@section('content')
    @include('layouts.headers.cards')

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Ships') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('ships.create') }}" class="btn btn-sm btn-primary">{{ __('Add ship') }}</a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-12">
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                    </div>

                    <div class="table-responsive">
                    <table id="myTable" class="table table-bordered dataTable no-footer">
                        <thead>
                          <tr>
                            <th> # </th>
                            <th> Name </th>
                            <th> Email </th>
                            <th> Mobile </th>
                            <th> Phone </th>
                            <th> Contact Information </th>
                           	<th> Action </th>
                          </tr>
                        </thead>
                        <tbody>
                            @php $i=1; @endphp
                            @foreach ($customers as $customers_data)
                        	<tr>
                                
                                <td>  {{ $i }} </td>
                                <td> {{ $customers_data->name }} </td>
                                <td> {{ $customers_data->email }} </td>
                                <td> {{ $customers_data->mobile }} </td>
                                <td> {{ $customers_data->phone }} </td>
                                <td>
                                   <ul>
                                       @if($customers_data->contact_name != NULL)
                                            Name: {{$customers_data->contact_name }} <br/>
                                       @endif
                                       @if($customers_data->contact_designation)
                                            Designation: {{ $customers_data->contact_designation }} <br/>
                                       @endif
                                            
                                       @if($customers_data->contact_phone)
                                            Phone: {{ $customers_data->contact_phone }} <br/>
                                       @endif
                                       @if($customers_data->contact_mobile)
                                            Mobile: {{ $customers_data->contact_mobile }} <br/>
                                       @endif
                                       @if($customers_data->contact_email)
                                            Email: {{ $customers_data->contact_email }} <br/>
                                       @endif
                                       @if($customers_data->contact_address)
                                            Address: {{ $customers_data->contact_address }} <br/>
                                       @endif
                                   </ul> 
                                </td>

                                <td>
                                    <a class="btn btn-sm btn-info" title="" data-original-title="Edit" href="{{ route('customers.edit',$customers_data->id) }}"><i class="fa fa-pencil-square-o"></i>Edit</a>
                                    {!! Form::open(['method' => 'DELETE','route' => ['customers.destroy', $customers_data->id],'style'=>'display:inline']) !!}
                                        <button class="btn btn-sm btn-danger" onclick="return deleteFunction()" title="" data-original-title="Delete"><i class="fa fa-trash-o"></i>Delete</button>
                                    {!! Form::close() !!}
                                </td>

                            @php $i++; @endphp
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
               

</div>
                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            
                        </nav>
                    </div>
                </div>
            </div>
        </div>
            
        @include('layouts.footers.auth')
    </div>
@endsection
@section('script')

    <script type="text/javascript">
        $(document).ready(function() {
            $("#myTable").dataTable();
            
        });
        function deleteFunction(){
                if(!confirm('Are you sure to delete this?'))
                event.preventDefault();
            }
    </script>

@stop
