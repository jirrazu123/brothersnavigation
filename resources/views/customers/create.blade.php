@extends('layouts.app', ['title' => __('Ship Entry')])

@section('content')
    @include('users.partials.header', ['title' => __('Ship Information')])   

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Ship') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('ships.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                       <h4 class="section-subtitle">
        <strong>Create Customer</strong>
        <a href="{{ route('customers.index')}}" class="btn btn-success btn-xs pull-right"><i class="fa fa-arrow-circle-left"></i> Customer List</a>
    </h4>

<div class="panel">
    <div class="panel-content">
        <form action="{{ route('customers.store') }}" method="post" accept-charset="utf-8">
            {{ csrf_field() }}
            @include('customers._form')

        </form>
    </div>
</div>
                    </div>
                </div>
            </div>
        </div>
        
        @include('layouts.footers.auth')
    </div>
@endsection



     


