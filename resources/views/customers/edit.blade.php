@extends('layouts.app', ['title' => __('Ship Entry')])

@section('content')
    @include('users.partials.header', ['title' => __('Ship Information')])   

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Ship') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('ships.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <h4 class="section-subtitle">
        <strong>Update Customer</strong>
        <a href="{{ route('customers.index')}}" class="btn btn-success pull-right btn-xs"><i class="fa fa-arrow-circle-left"></i> Customer List</a>
	</h4>

	{!! Form::model($customer, ['route'=>['customers.update', $customer->id], 'method' => 'PUT']) !!}
	<div class="panel">
		<div class="panel-content">

			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<label for="CustomerName" class="control-label"> Customer Name <span class="required" aria-required="true">*</span></label>
						{{Form::text('name',null,array('class' => 'form-control','required' => 'required'))}}
						@if ($errors->has('name'))
							<label id="email-error" class="error" for="name"> {{ $errors->first('name') }}</label>
						@endif
					</div>
					<div class="form-group">
						<label for="phone-mask"> Phone </label>
						<div class="input-group mb-sm">
							<span class="input-group-addon"><i class="fa fa-phone"></i></span>
							{{Form::text('phone',null,array('class' => 'form-control'))}}
						</div>
					</div>
					<div class="form-group">
						<label for="phone-mask"> Mobile </label>
						<div class="input-group mb-sm">
							<span class="input-group-addon"><i class="fa fa-phone"></i></span>
							{{Form::text('mobile',null,array('class' => 'form-control'))}}
							@if ($errors->has('mobile'))
								<label id="email-error" class="error" for="mobile"> {{ $errors->first('mobile') }}</label>
							@endif
						</div>
					</div>
				</div>

				<div class="col-sm-6">
					<div class="form-group">
						<label for="phone-mask"> Email</label>
						<div class="input-group mb-sm">
							<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
							{{Form::email('email',null,array('class' => 'form-control'))}}
							@if ($errors->has('email'))
								<label id="email-error" class="error" for="email"> {{ $errors->first('email') }}</label>
							@endif
						</div>
					</div>
					<!--Customer Address-->
					<div class="form-group">
						<label for="textareaMaxLength" class="control-label"> Customer Address </label>
						{{Form::textarea('address',null,array('class' => 'form-control', 'rows' => '5'))}}
					</div>
				</div>
			</div>
			<h4 class="section-subtitle"><b> Contact Person Information </b></h4>
			<hr>
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<label for="CustomerName" class="control-label"> Contact Name <span class="required" aria-required="true">*</span></label>
						{{Form::text('contact_name',null,array('class' => 'form-control'))}}
						@if ($errors->has('contact_name'))
							<label id="email-error" class="error" for="contact_name"> {{ $errors->first('contact_name') }}</label>
						@endif
					</div>
					<div class="form-group">
						<label for="Designation" class="control-label"> Designation </label>
						{{Form::text('contact_designation',null,array('class' => 'form-control'))}}
					</div>
					<div class="form-group">
						<label for="phone-mask"> Phone </label>
						<div class="input-group mb-sm">
							<span class="input-group-addon"><i class="fa fa-phone"></i></span>
							{{Form::text('contact_phone',null,array('class' => 'form-control'))}}
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label for="phone-mask"> Mobile </label>
						<div class="input-group mb-sm">
							<span class="input-group-addon"><i class="fa fa-phone"></i></span>
							{{Form::text('contact_mobile',null,array('class' => 'form-control'))}}
							@if ($errors->has('contact_mobile'))
								<label id="email-error" class="error" for="contact_mobile"> {{ $errors->first('contact_mobile') }}</label>
							@endif
						</div>
					</div>
					<div class="form-group">
						<label for="phone-mask"> Email </label>
						<div class="input-group mb-sm">
							<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                            <input type="email" class="form-control" name="contact_email" value="{{ $customer->contact_email }}">
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<h4 class="section-subtitle"><b> Invoice Address </b></h4>
					<div class="form-group">
						<label for="invoiceadd" class="control-label"> Invoice Address </label>
						{{Form::textarea('invoice_Address',null,array('class' => 'form-control', 'rows' => '3'))}}
					</div>
					<div class="form-group text-right">
						<button type="submit" class="btn btn-primary">
							Update Customer Information
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	{!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        
        @include('layouts.footers.auth')
    </div>
@endsection
 