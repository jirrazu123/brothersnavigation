@extends('layouts.app', ['title' => __('Create Trip Expense')])

@section('content')
    @include('users.partials.header', ['title' => __('Create Trip Expense')])   

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Create Trip Expenses') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('expenses.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('expenses.store') }}" autocomplete="off">
                            @csrf
                            
                            <h6 class="heading-small text-muted mb-4">{{ __('Create Trip Expense information') }}</h6>
                            <div class="pl-lg-4">
                            <div class="form-row">
                                
                            <div class="form-group col-md-12">
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Trip select</label>
                                    <select class="form-control" name="trip_id" id="trip_id" required="">
                                        <option value="">---Select Trip---</option>>
                                        @foreach($trips as $trip) 
                                            <option value="{{ $trip->id }}"
                                                {{ $trip->id==$trip_id ? 'selected' : '' }}>{{ $trip->name }} | Ship: | StartDate: {{ date('d:m:Y', strtotime($trip->start_date)) }} | Enddate: {{ date('d:m:Y', strtotime($trip->end_date)) }}  </option>
                                        @endforeach
                                    </select>
                                </div>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-12">
                                <div class="form-row">
                                    <div class="row col-md-10" id="expense_mult">
                                        
                                            <!-- js form add -->
                                            <div class="form-group col-md-4">
                                                <label class="form-control-label" for="head_id">{{ __('Expense Head') }}</label>
                                                <select class="form-control" name="head_id[]" id="head_id" required="">
                                                    <option value="">---select trip expense head ---</option>>
                                                    @foreach($expense_heads as $expense_head)
                                                        <option value="{{ $expense_head->id }}">{{ $expense_head->name }}</option>
                                                    @endforeach
                                                </select>
                                                
                                            </div>

                                            <div class="form-group col-md-3">
                                                <label class="form-control-label" for="amount">{{ __('Amount') }}</label>
                                                <input type="number" name="amount[]" id="amount" class="form-control form-control-alternative{{ $errors->has('amount') ? ' is-invalid' : '' }}" placeholder="{{ __('Enter amount') }}" value="" required="">
                                                
                                                @if ($errors->has('amount'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('amount') }}</strong>
                                                    </span>
                                                @endif
                                        
                                            </div>

                                            <!--remark--->
                                            <div class="form-group col-md-3">
                                                <label class="form-control-label" for="remark">{{ __('Remark') }}</label>
                                                <input type="text" name="remark[]" id="remark" class="form-control form-control-alternative" placeholder="{{ __('If any remark ... ') }}">

                                            </div>
                                            <!--//remark-->

                                            <!-- js from end -->
                                        
                                    </div>

                                    <div id="new_head" class="row col-md-10" style="display:none"> 
                                       
                                    </div>

                                    <div class="col-md-1 ">
                                        <p class="pt-3"></p>
                                        <span  id="add" class="btn btn-primary px-5" style="width:100%;"><i class="fas fa-plus"></i></span>
                                    </div>
                               
                            </div>
                           
                            
                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        @include('layouts.footers.auth')
    </div>
@endsection

@section('script')
<script type="text/javascript">
$(document).ready(function(){
  $(document).on('click', '#add',function(){
      $('#new_head').append(($('#expense_mult').html()));
      $('#new_head').append($('<div class="cross col-md-1 mt-5"><i class="fas fa-times btn btn-sm btn-warning"></i></div>'));
      $('#new_head').css('display',''); 
  })
  $(document).on('click', '.cross', function(){
    $(this).prev().remove();
    $(this).prev().remove();
    $(this).remove();
  }) 
});
</script>
@endsection