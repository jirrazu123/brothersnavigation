<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
    <div class="container-fluid">
        <!-- Toggler -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- Brand -->
        <a class="navbar-brand pt-0 text-center" href="{{ route('home') }}">
            <img src="{{ asset('argon') }}/img/brand/br-logo.png" class="navbar-brand-img" alt="...">
            <h3>Brother's Navigation</h3>
        </a>
        <!-- User -->
        <!-- <ul class="nav align-items-center d-md-none">
            <li class="nav-item dropdown">
                <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="media align-items-center">
                        <span class="avatar avatar-sm rounded-circle">
                        <img alt="Image placeholder" src="{{ asset('argon') }}/img/theme/team-1-800x800.jpg">
                        </span>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                    <div class=" dropdown-header noti-title">
                        <h6 class="text-overflow m-0">{{ __('Welcome!') }}</h6>
                    </div>
                    <a href="{{ route('profile.edit') }}" class="dropdown-item">
                        <i class="ni ni-single-02"></i>
                        <span>{{ __('My profile') }}</span>
                    </a>
                    <a href="#" class="dropdown-item">
                        <i class="ni ni-settings-gear-65"></i>
                        <span>{{ __('Settings') }}</span>
                    </a>
                    <a href="#" class="dropdown-item">
                        <i class="ni ni-calendar-grid-58"></i>
                        <span>{{ __('Activity') }}</span>
                    </a>
                    <a href="#" class="dropdown-item">
                        <i class="ni ni-support-16"></i>
                        <span>{{ __('Support') }}</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                        <i class="ni ni-user-run"></i>
                        <span>{{ __('Logout') }}</span>
                    </a>
                </div>
            </li>
        </ul> -->
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
            <!-- Collapse header -->
            <div class="navbar-collapse-header d-md-none">
                <div class="row">
                    <div class="col-6 collapse-brand">
                        <a href="{{ route('home') }}">
                            <img src="{{ asset('argon') }}/img/brand/blue.png">
                        </a>
                    </div>
                    <div class="col-6 collapse-close">
                        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                            <span></span>
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>
            <!-- Form -->
            <form class="mt-4 mb-3 d-md-none">
                <div class="input-group input-group-rounded input-group-merge">
                    <input type="search" class="form-control form-control-rounded form-control-prepended" placeholder="{{ __('Search') }}" aria-label="Search">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            <span class="fa fa-search"></span>
                        </div>
                    </div>
                </div>
            </form>
            <!-- Navigation -->
            <h6 class="navbar-heading text-muted">Launch</h6>
            <hr class="my-1">
            <ul class="navbar-nav">
                <!-- <li class="nav-item">
                    <a class="nav-link" href="{{ route('home') }}">
                        <i class="ni ni-tv-2 text-primary"></i> {{ __('Launch') }}
                    </a>
                </li> -->
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('basic_settings.edit',  array('id'=>1)) }}">
                        <!-- <i class="fas fa-cog" style="color: #f4645f;"></i> -->
                       <i class="fas fa-cog text-danger"></i>{{ __('Settings') }}
                    </a>

                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#navbar-ship" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-examples">
                        <i class="ni ni-ungroup text-orange"></i>
                        <span class="nav-link-text">Ship</span>
                    </a>
                    <div class="collapse" id="navbar-ship">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('ships.index') }}">
                                    <i class="fas fa-ship"></i></i> {{ __('Ships') }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('ship_expense_heads.index') }}">
                                    <i class="ni ni-key-25 text-info"></i></i> {{ __('Ships Expense Head') }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('ship_expenses.index') }}">
                                    <i class="fas fa-user text-primary"></i></i> {{ __('Ships expense') }}
                                </a>
                            </li>
                        
                        </ul>
                    </div>
                </li>
                
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('trips.index') }}">
                        <i class="fas fa-dolly-flatbed text-info"></i> {{ __('Ship Trips') }}
                    </a>
                </li>
                <a class="nav-link" href="#navbar-collection" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-examples">
                <!-- <i class="ni ni-ungroup text-orange"></i> -->
                <i class="fas fa-anchor text-orange"></i>
                <span class="nav-link-text">Trip Collections</span>
              </a>
                <div class="collapse" id="navbar-collection">
                <ul class="nav nav-sm flex-column">
                     
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('incomes.index') }}">
                            <i class="fas fa-dollar-sign text-success"></i> {{ __('Trips Income Head') }}
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('collections.index') }}">
                            <i class="fab fa-gg-circle text-primary"></i>{{ __('Trips Collection') }}
                        </a>
                    </li>
                </ul>
            </div>
                <a class="nav-link" href="#navbar-expnese" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-examples">
                <i class="ni ni-ungroup text-orange"></i>
                <span class="nav-link-text">Trip Expenses</span>
              </a>
                <div class="collapse" id="navbar-expnese">
                <ul class="nav nav-sm flex-column">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('expense_heads.index') }}">
                            <i class="ni ni-key-25 text-info"></i> {{ __('Trips Expense Head') }}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('expenses.index') }}">
                            <i class="fas fa-money-bill text-pink"></i> {{ __('Trips Expense') }}
                        </a>
                    </li>
                </ul>

                </div>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('managers.index')}}">
                        <i class="fas fa-user text-primary"></i> {{ __('Managers') }}
                    </a>
                </li>
            </ul>
                <!-- cargo Start -->
                    <h6 class="navbar-heading text-muted">Cargo/coaster</h6>
                    <hr class="my-1">
                   
               <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('cargos.index') }}">
                            <i class="fas fa-ship"></i></i> {{ __('Cargos/coasters') }}
                        </a>
                    </li>
                    
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('cargo_expense_heads.index') }}">
                            <i class="ni ni-key-25 text-info"></i></i> {{ __('Cargos/coasters Expense Head') }}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('cargo_expenses.index') }}">
                            <i class="fas fa-user text-primary"></i></i> {{ __('Cargos/coasters expense') }}
                        </a>
                    </li>
                      

                <li class="nav-item">
                    <a class="nav-link" href="{{ route('agencies.index') }}">
                    <i class="fas fa-id-badge text-primary"></i> {{ __('Cargos/coasters Agencies') }}
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('products.index') }}">
                    <i class="fab fa-product-hunt"></i> {{ __('Cargos/coasters products') }}
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('cargo_trips.index')}}">
                    <i class="fas fa-suitcase-rolling text-primary"></i> {{ __('Cargos/coasters Trip') }}
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('customers.index') }}">
                        <i class="ni ni-key-25 text-info"></i></i> {{ __('Cargos/coasters Customer') }}
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('demurrages.index') }}">
                    <i class="fab fa-product-hunt"></i> {{ __('Cargos/coasters Trip Demurrages') }}
                    </a>
                </li>
                <!-- //cargo end -->
            </ul>
            <ul class="navbar-nav">   
                <li class="nav-item">
                    <a class="nav-link" href="#">
                    <i class="fas fa-bug text-warning"></i> {{ __('Report') }}
                    </a>
                </li>
            </ul>
          
        </div>
    </div>
</nav>