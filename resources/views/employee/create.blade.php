@extends('layouts.app', ['title' => __('Create Employee')])

@section('content')
    @include('users.partials.header', ['title' => __('Create Employee Information')])   

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Create Employee') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('employees.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    @if($errors->any())
                    <div class="alert alert-danger">
                    <ul>
                    @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                    </ul>
                    </div>
                    @endif
                    <div class="card-body">
                        <form method="post" action="{{ route('employees.store') }}" autocomplete="off">
                            @csrf
                            
                            <div class="pl-lg-4">
                            <div class="form-row">

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="iname">{{ __('Name') }}</label>
                                    <input type="text" name="name" id="name" class="form-control form-control-alternative" placeholder="{{ __('Name') }}" value="" required autofocus>

                                </div>

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="mobile">{{ __('Mobile Number') }}</label>
                                    <input type="text" name="mobile" id="mobile" class="form-control form-control-alternative" placeholder="{{ __('Mobile Number') }}" value="" required autofocus>
                                </div>

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="ind">{{ __('NID') }}</label>
                                    <input type="text" name="nid" id="nid" class="form-control form-control-alternative" placeholder="{{ __('NID Number') }}" value="" required autofocus>
                                </div>

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="image">{{ __('Photo') }}</label>
                                    <input type="file" name="image" id="image" class="form-control form-control-alternative" placeholder="{{ __('Upload photo') }}" value="" required autofocus>
                                </div>

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="input-text">{{ __('Address') }}</label>
                                    <textarea rows="4" cols="50" name="address" id="address" class="form-control form-control-alternative" placeholder="{{ __('Presenrt Address') }}" required autofocus ></textarea>

                                </div>

                                <div class="form-group col-md-6">
                                    <label for="job_type">Job Type</label>
                                    <select class="form-control" name="job_type" id="job_type">
                                        <option value="">---Select JOB Type---</option>>
                                        <option value="">Full Time</option>
                                        <option value="">Part Time</option>
                                        <option value="">contractual</option>
                                       
                                    </select>
                                </div>

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="department">{{ __('Department') }}</label>
                                    <input type="text" name="department" id="department" class="form-control form-control-alternative" placeholder="{{ __('Department') }}" value="" required autofocus>
                                </div>

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="salary">{{ __('Salary') }}</label>
                                    <input type="text" name="salary" id="salary" class="form-control form-control-alternative" placeholder="{{ __('Salary') }}" value="" required autofocus>
                                </div>

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="joining_date">{{ __('Joining Date') }}</label>
                                    <input type="date" name="joining_date" id="joining_date" class="form-control form-control-alternative" placeholder="{{ __('Joining Date') }}" value="" required autofocus>
                                </div>

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="remark">{{ __('Remark') }}</label>
                                    <input type="text" name="remark" id="remark" class="form-control form-control-alternative" placeholder="{{ __('Writing text') }}" value="" required autofocus>
                                </div>

                                <div class="form-group col-md-12">
                                    <h3>Status</h3>
                                    @php
                                        if(@$product->status){
                                        $active = 'checked';
                                        $inactive = '';
                                        }else{
                                        $active = '';
                                        $inactive = 'checked';
                                    }
                                    @endphp
                                    
                                    <div class="custom-control custom-radio mb-3">
                                    <input name="status" class="custom-control-input" id="status1" type="radio" value="0" {{ $inactive }}>
                                    <label class="custom-control-label" for="status1">inactive</label>
                                    </div>
                                    <div class="custom-control custom-radio mb-3">
                                    <input name="status" class="custom-control-input" value="1" id="status2"  type="radio"  {{ $active.(!Request::segment(3)) ? 'checked' : ''}}>
                                    <label class="custom-control-label" for="status2">active</label>
                                    </div>
                                </div>
                                
                            </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        @include('layouts.footers.auth')
    </div>
@endsection