@extends('layouts.app', ['title' => __('Cargo/Coaster trip Entry')])

@section('content')
    @include('users.partials.header', ['title' => __('Cargo/Coaster Trip Information')])   

    <div class="container-fluid mt--7">
    
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Cargo/Coasterrgo Trip') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('cargo_trips.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{!! action('CargoTripController@update', ['id' => $cargo_trip->id]) !!}" autocomplete="off">
                            @csrf
                            <input type="hidden" name="_method" value="PUT">
                            @php
                            if($cargo_trip->trip_auto_id){
                                $trip_auto_id = $cargo_trip->trip_auto_id;
                            }else{
                                $date = Carbon\Carbon::today();
                                $t_id = count(App\Models\CargoTrip::all()) + 1;
                                $trip_auto_id = 'SOFTID-'.$date->year.$date->month.$date->day. '-'.str_pad($t_id,4,'0',STR_PAD_LEFT);
                            }
                            
                            @endphp

                            <div class="form-group">
                                <label for="po-auto">Trip Auto Genarate ID</label>
                                <input type="text" name="trip_auto_id" class="form-control" value="{{ $trip_auto_id }}" readonly>
                            </div>
                            <div class="pl-lg-4">
                            <div class="form-row">

                                <div class="form-group col-md-6">
                                    
                                    <div class="form-group">
                                        <label for="customer_id">Select Customer</label>
                                        <select class="form-control" name="customer_id" id="customer_id">
                                            <option value="">----Select Customer----</option>
                                            @foreach($customers as $customer)
                                              <option value="{{ $customer->id }}" {{ $cargo_trip->customer_id == $customer->id ? 'selected' : ''}}>{{$customer->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    

                                    @if ($errors->has('customer_id'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('customer_id') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group col-md-6">
                                    
                                    <div class="form-group">
                                        <label for="cargo_id">Select Cargo</label>
                                        <select class="form-control" name="cargo_id" id="cargo_id" required>
                                            <option value="">----Select Cargo----</option>
                                            @foreach($cargos as $cargo)
                                              <option value="{{$cargo->id}}" {{ $cargo_trip->cargo_id == $cargo->id ? 'selected' : '' }}>{{$cargo->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    

                                    @if ($errors->has('cargo_id'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('cargo_id') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group col-md-6">
                                    
                                    <div class="form-group">
                                        <label for="agency_id">Select Agency</label>
                                        <select class="form-control" name="agency_id" id="agency_id">
                                            <option value="">----Select Agency----</option>
                                            @foreach($agencys as $agency)
                                              <option value="{{$agency->id}}" {{ $cargo_trip->agency_id == $agency->id ? 'selected' : '' }}>{{$agency->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    

                                    @if ($errors->has('agency_id'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('agency_id') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group col-md-6">
                                    
                                    <div class="form-group">
                                        <label for="product_id">Select Product</label>
                                        <select class="form-control" name="product_id" id="product_id">
                                            <option value="">----Select Product----</option>
                                            @foreach($products as $product)
                                              <option value="{{$product->id}}" {{ $cargo_trip->product_id == $product->id ? 'selected' : '' }}>{{$product->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    

                                    @if ($errors->has('product_id'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('product_id') }}</strong>
                                        </span>
                                    @endif
                                </div>


                                <div class="form-group col-md-4">
                                    <label class="form-control-label" for="weight">{{ __('Weigth') }}</label>
                                    <input type="text" name="weight" value="{{ $cargo_trip->weight }}" min="0"  id="weight" class=" form-control form-control-alternative " placeholder="{{ __('Weight') }}" required autofocus>

                                </div>
                                <div class="form-group col-md-4">
                                    <label class="form-control-label" for="rate">{{ __('Rate') }}</label>
                                    <input type="number" name="rate" value="{{ $cargo_trip->rate }}" min="0"  id="rate" class=" form-control form-control-alternative " placeholder="{{ __('Rate') }}" required autofocus>

                                </div>
                                @php $amount = ($cargo_trip->weight*$cargo_trip->rate);@endphp
                                <div class="form-group col-md-4">
                                    <label class="form-control-label" for="result">{{ __('Amount') }}</label>
                                    <input type="number"  id="result" value="{{ $amount }}" class=" form-control form-control-alternative " placeholder="{{ __('amount') }}" readonly>
                                </div>

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="input-des_form">{{ __('Start Form') }}</label>
                                    <input type="text" name="des_form" value="{{ $cargo_trip->des_form }}" id="input-des_form" class=" form-control form-control-alternative " placeholder="{{ __('Start Form') }}" required autofocus>

                                </div>
                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="input-des_to">{{ __('End To') }}</label>
                                    <input type="text" name="des_to" value="{{ $cargo_trip->des_to }}" id="input-des_to" class=" form-control form-control-alternative  datepicker" placeholder="{{ __('End To') }}" required autofocus>

                                </div>
                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="selling_date">{{ __('Selling Date') }}</label>
                                    <input type="text" name="selling_date" value="{{ $cargo_trip->selling_date }}" id="selling_date" class=" form-control  form-control-alternative datepicker" placeholder="{{ __('Selling date ') }}"  required autofocus>

                                </div>
                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="unload_date">{{ __('Unload Date') }}</label>
                                    <input type="text" name="unload_date" value="{{ $cargo_trip->unload_date }}" id="unload_date" class=" form-control  form-control-alternative datepicker" placeholder="{{ __('Unload date ') }}"  required autofocus>

                                </div>
                                 
                                @php
                                    if(@$cargo_trip->status){
                                    $active = 'checked';
                                    $inactive = '';
                                    }else{
                                      $active = '';
                                      $inactive = 'checked';
                                   }
                                  @endphp

                                <div class="form-group col-md-12">
                                    <h3>Status</h3>
                                    <div class="custom-control custom-radio mb-3">
                                   
                                    <input name="status" class="custom-control-input" value="1" @php echo @$active @endphp id="radio-1" type="radio">
                                    <label class="custom-control-label" for="radio-1">Active</label>
                                    </div>
                                    <div class="custom-control custom-radio mb-3">
                                    <input name="status" class="custom-control-input" value='0' @php echo @$inactive @endphp  id="radio-2" type="radio">
                                    <label class="custom-control-label" for="radio-2">Inactive</label>
                                    </div>
                                </div>
                                
                                <!-- demurrage type -->
                                <div class="col-md-12">
                                <!-- <div class="custom-control custom-checkbox mb-3">
                                <input class="custom-control-input" id="damarage" name="is_damarage" value="1" type="checkbox" {{ $cargo_trip->is_damarage ? 'checked' : '' }}>
""                                    <label class="custom-control-label" for="damarage">Is Demurrage</label>
                                </div> -->
                                <div class="custom-control custom-checkbox mb-3">
                                    <input class="custom-control-input" id="isdamarage" name="is_damarage" value="1" type="checkbox"  {{ $cargo_trip->is_damarage ? 'checked' : '' }}>
                                     <label class="custom-control-label" for="isdamarage">Is Demurrage</label>
                                </div>

                                <div id="damarageForm" class="col-md-12 show-demarage-none">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                        
                                            <div class="form-group">
                                                <label for="cargo_id">Select Demurrage</label>
                                                <select class="form-control" name="demurrage_id" value="{{ $cargo_trip->demurrage_id }}" id="demurrage_id">
                                                    <option value="">----Select Demurrage----</option>
                                                    @foreach($damarages as $damarage)
                                                    <option value="{{$damarage->id}}" {{ $cargo_trip->demurrage_id == $damarage->id ? 'selected' : '' }}>{{$damarage->demurrage_type}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <label for="damarage-amount" class="form-control-label">Demurrage Amount</label>
                                            <input type="number" id="demarage-amount" name="demurrage_amount" value="{{ $cargo_trip->demurrage_amount }}" class="form-control">
                                        </div>

                                        <div class="col-md-6">
                                            <label for="damarage-date" class="form-control-label">Demurrage Date</label>
                                           
                                            <input type="text" id="damarage-date" name="demurrage_date" value="{{ $cargo_trip->demurrage_date }}" class="form-control  datepicker">
                                        </div>
                                        <div class="col-md-6">
                                            <label for="demurrage_received_date" class="form-control-label">Demurrage Received Date</label>
                                            <input type="text" id="demurrage_received_date" name="demurrage_received_date"  value="{{ $cargo_trip->demurrage_received_date }}" class="form-control datepicker"  onclick="initDatepicker">
                                        </div>

                                        <div class="form-group col-md-12 pt-3">
                                            <h3>Demurrage Status</h3>
                                            @php
                                                if(@$damarage->status){
                                                $active = 'checked';
                                                $inactive = '';
                                                }else{
                                                $active = '';
                                                $inactive = 'checked';
                                            }
                                            @endphp
                                            
                                            <div class="custom-control custom-radio mb-3">
                                            <input name="damarage_status" class="custom-control-input" id="damarage1" type="radio" value="0" data-checked="{{ $damarage->status ? 'checked' : ''}}" >
                                            <label class="custom-control-label" for="damarage1">inactive</label>
                                            </div>
                                            <div class="custom-control custom-radio mb-3">
                                            <input name="damarage_status" class="custom-control-input" value="1" id="damarage2"  type="radio"  data-checked="{{ $damarage->status ? 'checked' : ''}}" >
                                            <label class="custom-control-label" for="damarage2">active</label>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                 
                                </div>
                                <!-- //end demurrage type -->
                                
                            </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        @include('layouts.footers.auth')
    </div>
@endsection
@section('styles')
<link type="text/css" href="{{ asset('css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
<link type="text/css" href="{{ asset('css/razustyle.css') }}" rel="stylesheet">
@endsection

@section('script')
<script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(){
  var datepicker1 = $(".datepicker").datepicker();
  console.log(datepicker1);

  $(function(){
        $('#weight, #rate').keyup(function(){
            var value1 = parseFloat($('#weight').val()) || 0;
            var value2 = parseFloat($('#rate').val()) || 0;
            $('#result').val(value1*value2);
        });
    });
    

    $(document).on('change','#isdamarage',function() {
        console.log($(this));
        if($(this).is(":checked")) {
            // 
            document.getElementById("damarageForm").classList.add("show-demarage");
            document.getElementById("damarageForm").classList.remove("show-demarage-none");
           
        }
        else{
            
            document.getElementById("damarageForm").classList.remove("show-demarage");
            document.getElementById("damarageForm").classList.add("show-demarage-none");
            datepickers = document.getElementsByClassName("datepicker");
            
           
        }  
         
    });

    var dm1 = $("#damarage1").data("checked");
    var dm2 = $("#damarage2").data("checked");

    if(dm1 === "checked"){
        $("#damarage1").attr("checked",true);
    }

    else if(dm2 === "checked"){
        $("#damarage1").attr("checked",true);
    }


    function initDatepicker(){
        var id = $(this).id; 
       
    }

    $(".datepicker").datepicker({
        
    });
    

});
</script>
@endsection