@extends('layouts.app', ['title' => __('Cargo/Coaster Trip Management')])

@section('content')
    @include('layouts.headers.cards')

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Cargo/Coaster Trips') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('cargo_trips.create') }}" class="btn btn-sm btn-primary">{{ __('Add Cargo/Coaster  Trip') }}</a>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <div class="input-group input-group-alternative my-4">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-zoom-split-in  text-info"></i></span>
                                    </div>
                                    <input class="form-control form-control-alternative" placeholder="Search" type="text"><button class="btn btn-primary">Search</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4"></div>
                        </div>
                    </div>
                    <div class="col-12">
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                    </div>
                        
                    </div>

                        <!-- data on table -->

                        <table id="myTable" class="table table-bordered dataTable no-footer table-responsive">
                            <thead>
                            <tr>
                                <th> #SL </th>
                                <th> Cargo Name </th>
                                <th> Customer </th>
                                <th> Agency </th>
                                <th> Product </th>
                                <th> Wright </th>
                                <th> Rate </th>
                                <th> Amount </th>
                                <th> Start Form </th>
                                <th> End Form </th>
                                <th> Loaded Date </th>
                                <th> Unload Date </th>
                                <th> Action </th>
                            </tr>
                            </thead>
                            <tbody>
                                @php $i=1; @endphp
                                @foreach ($cargo_trips as $cargo_trip)
                                <tr>
                                
                                    <td>  {{ $i }} </td>
                                    <td> {{ @$cargo_trip->cargo->name }} </td>
                                    <td> {{ @$cargo_trip->customer->name }} </td>
                                    <td> {{ @$cargo_trip->agency->name }} </td>
                                    <td> {{ @$cargo_trip->product->name }} </td>
                                    <td> {{ $cargo_trip->weight }} </td>
                                    <td> {{ $cargo_trip->rate }} </td>
                                    @php $amount = ($cargo_trip->weight*$cargo_trip->rate);@endphp
                                    <td> {{ $amount }}</td>
                                    <td> {{ $cargo_trip->des_form }} </td>
                                    <td> {{ $cargo_trip->des_to }} </td>
                                    <td> {{  @date('d/m/Y', strtotime($cargo_trip->selling_date)) }} </td>
                                    <td> {{  @date('d/m/Y', strtotime($cargo_trip->unload_date)) }} </td>
                                    <td>
                                    <a class="btn btn-sm btn-info" title="" data-original-title="Edit" href="{{ route('cargo_trips.edit',$cargo_trip->id) }}"><i class="fa fa-pencil-square-o"></i>Edit</a>
                                    {!! Form::open(['method' => 'DELETE','route' => ['cargo_trips.destroy', $cargo_trip->id],'style'=>'display:inline']) !!}
                                        <button class="btn btn-sm btn-danger" title="" data-original-title="Delete"><i class="fa fa-trash-o"></i>Delete</button>
                                    {!! Form::close() !!}
                                    </td>

                                @php $i++; @endphp
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <!-- //data on table -->

                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            
                        </nav>
                    </div>
                </div>
            </div>
        </div>
            
        @include('layouts.footers.auth')
    </div>
@endsection