@extends('layouts.app', ['title' => __('Cargo Trips Management')])

@section('content')
    @include('layouts.headers.cards')

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Cargo Trips') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('cargo_trips.create') }}" class="btn btn-sm btn-primary">{{ __('Add Cargo Trips') }}</a>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <div class="input-group input-group-alternative my-4">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-zoom-split-in  text-info"></i></span>
                                    </div>
                                    <input class="form-control form-control-alternative" placeholder="Search" type="text"><button class="btn btn-primary">Search</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4"></div>
                        </div>
                    </div>
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                    </div>
                        <!-- Collapse -->
                        
                            <div id="accordion">
                             @foreach($cargo_trips as $cargo_trip)
                                <div class="card">
                                    <div class="card-header" id="heading-{{ $cargo_trip->id }}">
                                        <h5 class="mb-0">  
                                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapse-{{ $cargo_trip->id }}" aria-expanded="true" aria-controls="collapse-{{ $cargo_trip->id }}">
                                            Cargo Trip ID: <span class="text-default">{{ $cargo_trip->trip_auto_id }}</span> | </span>Cargo Name: <span class="text-success">{{ @$cargo_trip->cargo->name }} </span> <span class="px-3 text-warning"> | StartDate: {{ @date('d/m/Y', strtotime($cargo_trip->selling_date)) }} | Enddate: {{ @date('d/m/Y', strtotime($cargo_trip->unload_date)) }}</span> 
                                                                
                                            </button>
                                        </h5>
                                    </div>

                                    <div id="collapse-{{ $cargo_trip->id }}" class="collapse" aria-labelledby="heading-{{ $cargo_trip->id }}" data-parent="#accordion">
                                    <div class="card-body">

                                        <div class="table-responsive">
                                        <!-- Table start -->
                                        <table class="table">
                                            <thead class="thead-light">
                                                <tr>
                                                   
                                                    <th scope="col">{{ __('Particular') }}</th>
                                                    <th scope="col" class="text-right">{{ __('Amount') }}</th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                               
                                            
                                                <tr>
                                                @php $amount = ($cargo_trip->weight*$cargo_trip->rate);@endphp
                                                    <td>Total Amount<br><span class="text-danger">Total Expense</span><br></td>
                                                    <td class="text-right">{{ $amount }} BDT <br> <span class="text-danger">{{ $cargo_trip->total_expense }} BDT</span><br> </td>
                                                    
                                                </tr>
                                    
                                                <tr>
                                                    
                                                    <th class="text-primary bg-light">Grand Total</th>
                                                    <th class="text-right text-primary bg-light">{{ ($amount - $cargo_trip->total_expense) }} BDT</th>
                                                </tr>

                                                <tr class="text-right">
                                                    <td colspan="3">
                                                        <div class="dropdown">
                                                            <button class="btn btn-secondary dropdown-toggle bg-primary text-white" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            Option
                                                            </button>
                                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                                
                                                                @if(!$cargo_trip->is_expense)
                                                                <a class="dropdown-item" href="{{ route('cargo_expenses.create', ['id'=>$cargo_trip->id]) }}"><i class="fas fa-plus text-warning"></i>Add Expense</a>
                                                                @endif
                                                                <a class="dropdown-item" href="{{ route('cargo_trips.edit', array('id'=>$cargo_trip->id)) }}"><i class="fas fa-edit text-info"></i>Edit</a>
                                                                <form action="{{ route('cargo_trips.destroy',  array('id'=>$cargo_trip->id)) }}" method="POST">
                                                                @csrf 
                                                                <input type="hidden" name="_method" value="DELETE"><i class="fas fa-trash-alt text-danger pl-3"></i><button class="text-danger bg-white pl-3" onclick="return deleteFunction()" type="submit" style="border:none;">Delete</button>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                               
                                            </tbody>
                                            
                                        </table>
                                            <!-- Table end -->
                                        </div>

                                    </div>
                                    </div>
                                </div>
                             @endforeach    
                            </div>
                       
                        
                        <!-- Collapse end -->

                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            
                        </nav>
                    </div>
                </div>
            </div>
        </div>
            
        @include('layouts.footers.auth')
    </div>
@endsection
@section('script')
<script>
    function deleteFunction(){
        if(!confirm("Are you sure to delete this?"))
        event.preventDefault();
    }
</script>
@endsection