@extends('layouts.app', ['title' => __('Trip Management')])

@section('content')
    @include('layouts.headers.cards')

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Trips') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('trips.create') }}" class="btn btn-sm btn-primary">{{ __('Add trip') }}</a>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <div class="input-group input-group-alternative my-4">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-zoom-split-in  text-info"></i></span>
                                    </div>
                                    <input class="form-control form-control-alternative" placeholder="Search" type="text"><button class="btn btn-primary">Search</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4"></div>
                        </div>
                    </div>
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                    </div>
                        <!-- Collapse -->
                        
                            <div id="accordion">
                             @foreach($trips as $trip)
                                <div class="card">
                                    <div class="card-header" id="heading-{{ $trip->id }}">
                                        <h5 class="mb-0">  
                                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapse-{{ $trip->id }}" aria-expanded="true" aria-controls="collapse-{{ $trip->id }}">
                                            Trip ID: <span class="">{{ @$trip->trip_auto_id }}</span> | </span>Trip Name: <span class="text-success">{{ @$trip->name }} </span> | Ship: <span class="text-success">{{ @$trip->ship->name }} </span><span class="px-3 text-warning"> | StartDate: {{ @date('d/m/Y', strtotime($trip->start_date)) }} | Enddate: {{ @date('d/m/Y', strtotime($trip->end_date)) }}</span> 
                                                                
                                            </button>
                                        </h5>
                                    </div>

                                    <div id="collapse-{{ $trip->id }}" class="collapse" aria-labelledby="heading-{{ $trip->id }}" data-parent="#accordion">
                                    <div class="card-body">

                                        <div class="table-responsive">
                                        <!-- Table start -->
                                        <table class="table">
                                            <thead class="thead-light">
                                                <tr>
                                                   
                                                    <th scope="col">{{ __('Particular') }}</th>
                                                    <th scope="col" class="text-right">{{ __('Amount') }}</th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    
                                                    <td>UP Collection <br>Down Collection<br> <hr class="my-2"><span class="text-success">Total Collection </span><br><span class="text-danger">Total Expense</span> <br><hr class="my-2"><span class="text-success"> Net Balance </span><br><span class="text-info">Other's Income</span></td>
                                                    <td class="text-right">{{ $trip->total_up_collection}} BDT <br>{{ $trip->total_down_collection}} BDT<br> <hr class="my-2"> <span class="text-success">{{ $trip->total_up_collection+$trip->total_down_collection}} BDT </span><br><span class="text-danger">{{ $trip->total_expense}} BDT</span> <br><hr class="my-2"><span class="text-success">{{ ($trip->total_up_collection+$trip->total_down_collection)-$trip->total_expense}} BDT</span><br><span class="text-info"> {{ $trip->total_other_income}} BDT</span></td>
                                                    
                                                </tr>
                                    
                                                <tr>
                                                    
                                                    <th class="text-primary bg-light">Grand Total</th>
                                                    <th class="text-right text-primary bg-light">{{ ($trip->total_up_collection+$trip->total_down_collection+$trip->total_other_income)-$trip->total_expense}} BDT</th>
                                                </tr>
                                                
                                                <tr class="text-right">
                                                    <td colspan="3">
                                                        <div class="dropdown">
                                                            <button class="btn btn-secondary dropdown-toggle bg-primary text-white" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            Option
                                                            </button>
                                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                                @if(!$trip->is_collection)
                                                                <a class="dropdown-item" href="{{ route('collections.create', ['id'=>$trip->id]) }}"><i class="fas fa-plus text-success"></i>Add Collection</a>
                                                                @endif
                                                                @if(!$trip->is_expense)
                                                                <a class="dropdown-item" href="{{ route('expenses.create', ['id'=>$trip->id]) }}"><i class="fas fa-plus text-warning"></i>Add Expense</a>
                                                                @endif
                                                                <a class="dropdown-item" href="{{ route('trips.edit', array('id'=>$trip->id)) }}"><i class="fas fa-edit text-info"></i>Edit</a>
                                                                <form action="{{ route('trips.destroy',  array('id'=>$trip->id)) }}" method="POST">
                                                                @csrf 
                                                                <input type="hidden" name="_method" value="DELETE"><i class="fas fa-trash-alt text-danger pl-3"></i><button class="text-danger bg-white pl-3" onclick="return deleteFunction()" type="submit" style="border:none;">Delete</button>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>

                                            </tbody>
                                            
                                        </table>
                                            <!-- Table end -->
                                        </div>

                                    </div>
                                    </div>
                                </div>
                             @endforeach    
                            </div>
                       
                        
                        <!-- Collapse end -->

                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            {{ $trips->links() }}
                        </nav>
                    </div>
                </div>
            </div>
        </div>
            
        @include('layouts.footers.auth')
    </div>
@endsection
@section('script')
<script>
    function deleteFunction(){
        if(!confirm('Are you sure to delete this?'))
        event.preventDefault();
    }
</script>
@endsection