@extends('layouts.app', ['title' => __('Create trip Entry')])

@section('content')
    @include('users.partials.header', ['title' => __('Create Trip Information')])   

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Create Trip') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('trips.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{route('trips.store')}}" autocomplete="off">
                            @csrf
                            <div class="form-group">
                                <label for="po-auto">Trip Auto Genarate ID</label>
                                <input type="text" name="trip_auto_id" class="form-control" value="{{ $trip_id }}" readonly>
                            </div>
                            <div class="pl-lg-4">
                            <div class="form-row">
                            
                            <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }} col-md-6">

                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Trip Name</label>
                                    <input type="text" name="name" id="input-name" class="form-control form-control-alternative" placeholder="{{ __('Enter your Trip name') }}"  required autofocus>
                                </div>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>

                                <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }} col-md-6">
                                    
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Ship select</label>
                                        <select class="form-control" name="ship_id" id="ship_id">
                                            @foreach($ships as $ship)
                                              <option value="{{$ship->id}}">{{$ship->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    @if ($errors->has('ship_id'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('ship_id') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="input-name">{{ __('Start Date') }}</label>
                                    <input type="date" name="start_date" id="input-number" class=" form-control form-control-alternative " placeholder="{{ __('select date') }}" value="{{ old('number') }}" required autofocus>

                                   
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="input-name">{{ __('End Date') }}</label>
                                    <input type="date" name="end_date" id="d" class=" form-control  form-control-alternative" placeholder="{{ __('select date ') }}"  required autofocus>

                                </div>

                                <div class="form-group col-md-12">
                                <h3>Status</h3>
                                @php
                                    if(@$product->status){
                                    $active = 'checked';
                                    $inactive = '';
                                    }else{
                                      $active = '';
                                      $inactive = 'checked';
                                   }
                                  @endphp
                                  
                                  <div class="custom-control custom-radio mb-3">
                                  <input name="status" class="custom-control-input" id="status1" type="radio" value="0" {{ $inactive }}>
                                  <label class="custom-control-label" for="status1">inactive</label>
                                </div>
                                <div class="custom-control custom-radio mb-3">
                                  <input name="status" class="custom-control-input" value="1" id="status2"  type="radio"  {{ $active.(!Request::segment(3)) ? 'checked' : ''}}>
                                  <label class="custom-control-label" for="status2">active</label>
                                </div>
                            </div>
                                
                            </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        @include('layouts.footers.auth')
    </div>
@endsection
@section('styles')
<link type="text/css" href="{{ asset('css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
@endsection

@section('script')
<script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(){
  $(".datepicker").datepicker({
      autoclose: true
  });
});
</script>
@endsection