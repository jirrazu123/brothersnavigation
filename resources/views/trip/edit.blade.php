@extends('layouts.app', ['title' => __('Update trip Entry')])

@section('content')
    @include('users.partials.header', ['title' => __('Update Trip Information')])   

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Update Trip') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('trips.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{!! action('TripController@update', ['id' => $trip->id]) !!}" autocomplete="off">
                            @csrf
                            <input type="hidden" name="_method" value="PUT">
                            @php
                            if($trip->trip_auto_id){
                                $trip_auto_id = $trip->trip_auto_id;
                            }else{
                                $date = Carbon\Carbon::today();
                                $t_id = count(App\Models\Trip::all()) + 1;
                                $trip_auto_id = 'SOFTID-'.$date->year.$date->month.$date->day. '-'.str_pad($t_id,4,'0',STR_PAD_LEFT);
                            }
                            
                            @endphp

                            <div class="form-group">
                                <label for="po-auto">Trip Auto Genarate ID</label>
                                <input type="text" name="trip_auto_id" class="form-control" value="{{ $trip_auto_id }}" readonly>
                            </div>

                            <div class="pl-lg-4">
                            <div class="form-row">

                            <div class="form-group col-md-6">
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Trip Name</label>
                                    <input type="text" name="name" id="name" value="{{ $trip->name }}" class="form-control form-control-alternative" placeholder="{{ __('Enter your Trip name') }}"  required autofocus>
                                </div>
                            </div>

                                <div class="form-group col-md-6">
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Ship select</label>
                                        <select class="form-control" name="ship_id" id="ship_id">
                                            @foreach($ships as $ship)
                                              <option value="{{$ship->id}}">{{$ship->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="input-name">{{ __('Start Date') }}</label>
                                    <input type="date" name="start_date" value="{{ date('Y-m-d', strtotime($trip->start_date)) }}" id="start_date" class="form-control form-control-alternative" placeholder="{{ __('Number of sits') }}" required autofocus>
                                </div>

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="input-name">{{ __('End Date') }}</label>
                                    <input type="date" name="end_date" value="{{ date('Y-m-d', strtotime($trip->end_date)) }}" id="end_date" class="form-control form-control-alternative" placeholder="{{ __('Number of sits') }}"  required autofocus>
                                </div>
                                
                                <div class="form-group col-md-12">
                                    <h3>Status</h3>
                                    @php
                                        if(@$trip->status){
                                            $active='checked';
                                            $inactive='';
                                        }else
                                        {
                                            $active='';
                                            $inactive='chacked';
                                        }
                                    @endphp
                                    <div class="custom-control custom-radio mb-3">
                                        <input name="status" class="custom-control-input" value="1" @php echo @$active @endphp id="radio-1" type="radio">
                                        <label class="custom-control-label" for="radio-1">Active</label>
                                        </div>
                                        <div class="custom-control custom-radio mb-3">
                                        <input name="status" class="custom-control-input" value='0' @php echo @$inactive @endphp  id="radio-2" type="radio">
                                        <label class="custom-control-label" for="radio-2">Inactive</label>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        @include('layouts.footers.auth')
    </div>
@endsection