@extends('layouts.app', ['title' => __('Update Ship Entry')])

@section('content')
    @include('users.partials.header', ['title' => __('Update Ship Information')])   

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Update Ship') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('ships.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{!! action('ShipController@update', ['id' => $ship->id]) !!}" >
                            
                            @csrf
                            <input type="hidden" name="_method" value="PUT">
                            <div class="pl-lg-4">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="input-name">{{ __('Name') }}</label>
                                    <input type="text" name="name" id="input-name" value="{{ $ship->name }}"class="form-control form-control-alternative" placeholder="{{ __('Name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="input-name">{{ __('Deluxe') }}</label>
                                    <input type="number" name="deluxe" id="input-number" value="{{ $ship->deluxe }}" class="form-control form-control-alternative" placeholder="{{ __('Number of sit') }}" required autofocus>

                                    @if ($errors->has('delux'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('deluxe') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="input-name">{{ __('Cabin') }}</label>
                                    <input type="number" name="cabin" id="input-number" value="{{ $ship->cabin }}" class="form-control form-control-alternative" placeholder="{{ __('Number of sit') }}"  required autofocus>

                                    @if ($errors->has('cabin'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('cabin') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="input-name">{{ __('Dak') }}</label>
                                    <input type="number" name="dak" id="input-number" value="{{ $ship->dak }}" class="form-control form-control-alternative" placeholder="{{ __('Number of sit') }}"  required autofocus>

                                    @if ($errors->has('dak'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('dak') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                @php
                                    if(@$ship->status){
                                    $active = 'checked';
                                    $inactive = '';
                                    }else{
                                      $active = '';
                                      $inactive = 'checked';
                                   }
                                  @endphp

                                <div class="form-group col-md-12">
                                    <h3>Status</h3>
                                    <div class="custom-control custom-radio mb-3">
                                   
                                    <input name="status" class="custom-control-input" value="1" @php echo @$active @endphp id="radio-1" type="radio">
                                    <label class="custom-control-label" for="radio-1">Active</label>
                                    </div>
                                    <div class="custom-control custom-radio mb-3">
                                    <input name="status" class="custom-control-input" value='0' @php echo @$inactive @endphp  id="radio-2" type="radio">
                                    <label class="custom-control-label" for="radio-2">Inactive</label>
                                    </div>
                                </div>
                               
                                
                            </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        @include('layouts.footers.auth')
    </div>
@endsection