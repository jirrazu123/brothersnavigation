@extends('layouts.app', ['title' => __('Basic Setting')])

@section('content')
    @include('layouts.headers.cards')

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Basic Setting') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('basic_settings.create') }}" class="btn btn-sm btn-primary">{{ __('Add Basic setting') }}</a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-12">
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                    </div>
                        <!-- Collapse -->
                        <div class="container">
                            <div class="table-responsive">
                                <table class="table table-borderless">
                                <tr>
                                    <th>Company Name : Brother's Navigation</th>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th>Company logo :</th>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th>Address :</th>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th>Contact :</th>
                                    <td></td>
                                </tr>
                                </table>
                            </div>
                        </div>
                        
                        <!-- Collapse end -->

                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            
                        </nav>
                    </div>
                </div>
            </div>
        </div>
            
        @include('layouts.footers.auth')
    </div>
@endsection