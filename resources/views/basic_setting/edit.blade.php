@extends('layouts.app', ['title' => __('Basic Setting Entry')])

@section('content')
    @include('users.partials.header', ['title' => __('Basic Setting')])   

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Basic Setting') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{!! action('BasicSettingController@update', ['id' => $basic_setting->id]) !!}" autocomplete="off">
                              @csrf
                                <input type="hidden" name="_method" value="PUT">
                                <img src="{{ asset('argon') }}/img/brand/br-logo.png" class="img-fluid" alt="..." style="width:150px; height:150px">
                                <!-- <h3>Brother's Navigation</h3> -->
                            
                            <!-- <h6 class="heading-small text-muted mb-4">{{ __('Basic Setting') }}</h6> -->
                            <div class="pl-lg-4">
                            <div class="form-row">

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="input-name">{{ __('Company Name') }}</label>
                                    <input type="text" name="company_name" id="input-name" value="{{ $basic_setting->company_name }}" class="form-control form-control-alternative" placeholder='{{ __("Brothers Navigation") }}' required autofocus  >
                                </div>

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="input-name">{{ __('Logo') }}</label>
                                    <input type="file" name="logo" value="{{ $basic_setting->logo }}" class="form-control form-control-file" id="file">
                                    @if ($errors->has('file'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('file') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="input-text">{{ __('Address') }}</label>
                                    <textarea rows="4" cols="50" name="address" id="input-text" value="{{ $basic_setting->address }}" class="form-control form-control-alternative" placeholder="{{ __('Address') }}" required autofocus >{{ $basic_setting->address }}</textarea>

                                </div>

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="input-text">{{ __('Contact') }}</label>
                                    <input type="text" name="contact" id="input-text" value="{{ $basic_setting->contact }}" class="form-control form-control-alternative" placeholder="{{ __('Contact') }}"  required autofocus >

                                </div>

                            </div>

                                <div class="text-right">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        @include('layouts.footers.auth')
    </div>
@endsection