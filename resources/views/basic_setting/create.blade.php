@extends('layouts.app', ['title' => __('Basic Setting Entry')])

@section('content')
    @include('users.partials.header', ['title' => __('Basic Setting')])   

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Basic Setting') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <!-- <a href="{{ route('basic_settings.index') }}" class="btn btn-sm btn-primary">{{ __('Back to index') }}</a> -->
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('user.store') }}" autocomplete="off">
                            @csrf
                            
                            <h6 class="heading-small text-muted mb-4">{{ __('Basic Setting') }}</h6>
                            <div class="pl-lg-4">
                            <div class="form-row">

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="input-name">{{ __('Company Name') }}</label>
                                    <input type="text" name="name" id="input-name" class="form-control form-control-alternative{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder='{{ __("Brothers Navigation") }}' required autofocus  disabled>
                                </div>

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="input-name">{{ __('Logo') }}</label>
                                    <input type="file" class="form-control form-control-file" id="file">
                                    @if ($errors->has('file'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('file') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="input-text">{{ __('Address') }}</label>
                                    <input type="text" name="text" id="input-text" class="form-control form-control-alternative{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="{{ __('Address') }}" required autofocus disabled>

                                </div>

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="input-text">{{ __('Contact') }}</label>
                                    <input type="text" name="text" id="input-text" class="form-control form-control-alternative{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="{{ __('Contact') }}" value="{{ old('Contact') }}" required autofocus disabled>

                                </div>

                            </div>

                                <div class="text-right">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        @include('layouts.footers.auth')
    </div>
@endsection