@extends('layouts.app', ['title' => __('Update Cargo Expense Head')])

@section('content')
    @include('users.partials.header', ['title' => __('Update Cargo Expense Head Information')])   

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Update Cargo Expense Head') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('cargo_expense_heads.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{!! action('CargoExpenseHeadController@update', ['id' => $cargo_expense_head->id]) !!}" autocomplete="off">
                            @csrf
                            <input type="hidden" name="_method" value="PUT">
                            <div class="pl-lg-4">
                            <div class="form-row">

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="input-name">{{ __('Name') }}</label>
                                    <input type="text" name="name" id="input-name" value="{{ $cargo_expense_head->name }}" class="form-control form-control-alternative" placeholder="{{ __('Name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                @php
                                    if(@$cargo_expense_head->status){
                                    $active = 'checked';
                                    $inactive = '';
                                    }else{
                                      $active = '';
                                      $inactive = 'checked';
                                   }
                                  @endphp

                                <div class="form-group col-md-12">
                                    <h3>Status</h3>
                                    <div class="custom-control custom-radio mb-3">
                                   
                                    <input name="status" class="custom-control-input" value="1" @php echo @$active @endphp id="radio-1" type="radio">
                                    <label class="custom-control-label" for="radio-1">Active</label>
                                    </div>
                                    <div class="custom-control custom-radio mb-3">
                                    <input name="status" class="custom-control-input" value='0' @php echo @$inactive @endphp  id="radio-2" type="radio">
                                    <label class="custom-control-label" for="radio-2">Inactive</label>
                                    </div>
                                </div>
                                
                            </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        @include('layouts.footers.auth')
    </div>
@endsection