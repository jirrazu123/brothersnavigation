@extends('layouts.app', ['title' => __('Create Manager')])

@section('content')
    @include('users.partials.header', ['title' => __('Create Manager Information')])   

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Create Manager') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('managers.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('managers.store') }}" autocomplete="off">
                            @csrf
                            
                            <h6 class="heading-small text-muted mb-4">{{ __('Create Manager information') }}</h6>
                            <div class="pl-lg-4">
                            <div class="form-row">

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="input-name">{{ __('Name') }}</label>
                                    <input type="text" name="name" id="name" class="form-control form-control-alternative" placeholder="{{ __('Name') }}" value="" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="input-name">{{ __('Mobile Number') }}</label>
                                    <input type="text" name="mobile" id="mobile" class="form-control form-control-alternative" placeholder="{{ __('Mobile Number') }}" value="" required autofocus>

                                    @if ($errors->has('mobile'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('mobile') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="input-name">{{ __('NID') }}</label>
                                    <input type="text" name="nid" id="nid" class="form-control form-control-alternative" placeholder="{{ __('NID Number') }}" value="" required autofocus>

                                    @if ($errors->has('nid'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('nid') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="input-name">{{ __('Photo') }}</label>
                                    <input type="file" name="photo" id="photo" class="form-control form-control-alternative" placeholder="{{ __('Upload photo') }}" value="" required autofocus>

                                    @if ($errors->has('file'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('file') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="input-text">{{ __('Present Address') }}</label>
                                    <textarea rows="4" cols="50" name="present_address" id="present_address" class="form-control form-control-alternative" placeholder="{{ __('Presenrt Address') }}" required autofocus ></textarea>

                                </div>

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="input-text">{{ __('Permanent Address') }}</label>
                                    <textarea rows="4" cols="50" name="permanent_address" id="permanent_address" class="form-control form-control-alternative" placeholder="{{ __('Permanetn Address') }}" required autofocus ></textarea>

                                </div>

                                <div class="form-group col-md-12">
                                    <h3>Status</h3>
                                    @php
                                        if(@$product->status){
                                        $active = 'checked';
                                        $inactive = '';
                                        }else{
                                        $active = '';
                                        $inactive = 'checked';
                                    }
                                    @endphp
                                    
                                    <div class="custom-control custom-radio mb-3">
                                    <input name="status" class="custom-control-input" id="status1" type="radio" value="0" {{ $inactive }}>
                                    <label class="custom-control-label" for="status1">inactive</label>
                                    </div>
                                    <div class="custom-control custom-radio mb-3">
                                    <input name="status" class="custom-control-input" value="1" id="status2"  type="radio"  {{ $active.(!Request::segment(3)) ? 'checked' : ''}}>
                                    <label class="custom-control-label" for="status2">active</label>
                                    </div>
                                </div>
                                
                            </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        @include('layouts.footers.auth')
    </div>
@endsection