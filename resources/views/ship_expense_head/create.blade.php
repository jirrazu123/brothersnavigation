@extends('layouts.app', ['title' => __('Create Ship Expense Head')])

@section('content')
    @include('users.partials.header', ['title' => __('Create Ship Expense Head')])   

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Create Ship Expense Head') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('ship_expense_heads.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('ship_expense_heads.store') }}" autocomplete="off">
                            @csrf
                            
                            <h6 class="heading-small text-muted mb-4">{{ __('Create Ship Expense Head information') }}</h6>
                            <div class="pl-lg-4">
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label class="form-control-label" for="input-name">{{ __('Name') }}</label>
                                    <input type="text" name="name" id="name" class="form-control form-control-alternative" placeholder="{{ __('Name') }}" value="" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            
                            <div class="form-group col-md-12">
                                <h3>Status</h3>
                                @php
                                    if(@$product->status){
                                    $active = 'checked';
                                    $inactive = '';
                                    }else{
                                      $active = '';
                                      $inactive = 'checked';
                                   }
                                  @endphp
                                  
                                  <div class="custom-control custom-radio mb-3">
                                  <input name="status" class="custom-control-input" id="status1" type="radio" value="0" {{ $inactive }}>
                                  <label class="custom-control-label" for="status1">inactive</label>
                                </div>
                                <div class="custom-control custom-radio mb-3">
                                  <input name="status" class="custom-control-input" value="1" id="status2"  type="radio"  {{ $active.(!Request::segment(3)) ? 'checked' : ''}}>
                                  <label class="custom-control-label" for="status2">active</label>
                                </div>
                            </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        @include('layouts.footers.auth')
    </div>
@endsection