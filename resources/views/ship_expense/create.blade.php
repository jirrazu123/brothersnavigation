@extends('layouts.app', ['title' => __('Create Ship Expense')])

@section('content')
    @include('users.partials.header', ['title' => __('Create Ship Expense')])   

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Create Ship Expenses') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('ship_expenses.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('ship_expenses.store') }}" autocomplete="off">
                            @csrf
                            
                            <h6 class="heading-small text-muted mb-4">{{ __('Create Ship Expense information') }}</h6>
                            <div class="form-row">
                                
                                <div class="form-group col-md-6">
                                    <label for="exampleFormControlSelect1">Select Ship</label>
                                    <select class="form-control" name="ship_id" id="ship_id">
                                        @foreach($ships as $ship)
                                            <option value="{{ $ship->id }}">{{ $ship->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="exampleFormControlSelect1">Expense Head</label>
                                    <select class="form-control" name="head_id" id="expense_head_id">
                                        @foreach($ship_expense_heads as $ship_expense_head)
                                            <option value="{{ $ship_expense_head->id }}">{{ $ship_expense_head->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Expense Date</label>
                                    <input type="date" class="form-control" name="expense_date">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="amount">Amount</label>
                                    <input type="text" class="form-control" name="amount" >
                                </div>
                           
                            
                                <div class="form-group py-3 ml-auto">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        @include('layouts.footers.auth')
    </div>
@endsection
@section('styles')
<link type="text/css" href="{{ asset('css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
@endsection

@section('script')
<script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(){
  $(".datepicker").datepicker({
      autoclose: true
  });
});
</script>
@endsection