@extends('layouts.app', ['title' => __('Ship Expense Management')])

@section('content')
    @include('layouts.headers.cards')

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Ship Expense') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('ship_expenses.create') }}" class="btn btn-sm btn-primary">{{ __('Add Ship Expense') }}</a>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <div class="input-group input-group-alternative my-4">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-zoom-split-in  text-info"></i></span>
                                    </div>
                                    <input class="form-control form-control-alternative" placeholder="Search" type="text"><button class="btn btn-primary">Search</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4"></div>
                        </div>
                    </div>
                    
                    <div class="col-12">
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                    </div>
                    
                    <div class="table-responsive p-4">
                        <table class="table table-flush" id="datatable-basic">
                            <thead class="thead-light">
                                <tr>
                                    <th>Date</th>
                                    <th>Ship Name</th>
                                    <th>Expense Head</th>
                                    <th>Expense Amount</th>
                                    <th></th>
                                    <th class="text-right">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($expenses as $k=> $expense)
                                    <tr>
                                        <td>{{ @$expense->expense_date }}</td>
                                        <td>{{ @$expense->ship->name }}</td>
                                        <td>{{ @$expense->shipExpenseHead->name }}</td>
                                        <td> {{ @$expense->amount }} <span><small> &nbsp;BDT</small></span></td>
                                        <td class="text-right">
                                            <a href="{{ route('ship_expenses.edit',  array('id'=>@$expense->id)) }}" class="btn btn-sm btn-info">Edit</a>
                                        </td>
                                        <td>
                                            <form action="{{ route('ship_expenses.destroy',  array('id'=>@$expense->id)) }}" method="POST">
                                                @csrf 
                                                <input type="hidden" name="_method" value="DELETE">
                                                <button class="btn btn-sm btn-danger" onclick="return deleteFunction()" type="submit">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                             {{ $expenses->links() }}
                        </nav>
                    </div>
                </div>
            </div>
        </div>
            
        @include('layouts.footers.auth')
    </div>
@endsection
@section('script')
<script>
    function deleteFunction(){
        if(!confirm("Are you sure to delete this?"))
        event.preventDefault();
    }
</script>
@endsection