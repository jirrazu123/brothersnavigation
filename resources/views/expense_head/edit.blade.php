@extends('layouts.app', ['title' => __('Update Expense Head')])

@section('content')
    @include('users.partials.header', ['title' => __('Update  Expense Head')])   

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Update Expense Head') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('expense_heads.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{!! action('ExpenseHeadController@update', ['id' => $expense_head->id]) !!}" autocomplete="off">
                            @csrf
                            <input type="hidden" name="_method" value="PUT">
                            <h6 class="heading-small text-muted mb-4">{{ __('Update Expense Head information') }}</h6>
                            <div class="pl-lg-4">
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label class="form-control-label" for="input-name">{{ __('Name') }}</label>
                                    <input type="text" name="name" value="{{ $expense_head->name }}" id="name" class="form-control form-control-alternative" placeholder="{{ __('Name') }}"  required autofocus>
                                </div>
                            </div>
                            
                            <div class="form-group col-md-12">
                                <h3>Status</h3>
                                <div class="custom-control custom-radio mb-3">
                                <input name="status" class="custom-control-input" value='1' id="radio-1" checked="" type="radio">
                                <label class="custom-control-label" for="customRadio5">Active</label>
                                </div>
                                <div class="custom-control custom-radio mb-3">
                                <input name="status" class="custom-control-input" value='0' id="radio-0" type="radio">
                                <label class="custom-control-label" for="customRadio6">Inactive</label>
                                </div>
                            </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        @include('layouts.footers.auth')
    </div>
@endsection