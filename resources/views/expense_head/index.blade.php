@extends('layouts.app', ['title' => __('Trip Expanse Head Information')])

@section('content')
    @include('layouts.headers.cards')

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Trip Expanse Head') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('expense_heads.create') }}" class="btn btn-sm btn-primary">{{ __('Add Trip Expanse Head') }}</a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-12">
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                    </div>

                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th>{{ __('#SL') }}</th>
                                    <th>{{ __('Name') }}</th>
                                    <th>{{ __('Status') }}</th>
                                    <th class="text-right">{{__('Action') }}</th>
                                    <th></th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                            @php
                                @$status=['inactive', 'active'];
                            @endphp
                                @foreach($expense_heads as $key=>$expense_head)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $expense_head->name }}</td>
                                        <td>{{ $status[$expense_head->status] }}</td>
                                       
                                        <td class="text-right">
                                            <a href="{{route('expense_heads.edit', array('id'=>$expense_head->id)) }}" class="btn btn-sm btn-info">Edit</a>
                                            
                                        </td>

                                        <td>
                                            <form action="{{ route('expense_heads.destroy',  array('id'=>$expense_head->id)) }}" method="POST">
                                            @csrf 
                                            <input type="hidden" name="_method" value="DELETE">
                                            <button class="btn btn-sm btn-danger" onclick="return deleteFunction()" type="submit">Delete</button>
                                            </form>
                                        </td>
                                       
                                    </tr>
                                @endforeach
                                   
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            
                        </nav>
                    </div>
                </div>
            </div>
        </div>
            
        @include('layouts.footers.auth')
    </div>
@endsection
@section('script')
<script>
  function deleteFunction(){
      if(!confirm("Are you sure to delete this?"))
      event.preventDefault();
  }
</script>
@endsection