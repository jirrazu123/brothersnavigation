@extends('layouts.app', ['title' => __('Get Cargo Trips Demurrage Management')])

@section('content')
    @include('layouts.headers.cards')

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Get Cargo Trips Demurrage') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <!-- <a href="" class="btn btn-sm btn-primary">{{ __('Add Get Cargo Trips Demurrage') }}</a> -->
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <div class="input-group input-group-alternative my-4">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-zoom-split-in  text-info"></i></span>
                                    </div>
                                    <input class="form-control form-control-alternative" placeholder="Search" type="text"><button class="btn btn-primary">Search</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4"></div>
                        </div>
                    </div>
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                    </div>
                        <!-- data on table -->

                        <table id="myTable" class="table table-bordered dataTable no-footer table-responsive p-2">
                            <thead>
                            <tr>
                                <th> #SL </th>
                                <th> cargo Trip Information</th>
                                <th> Demurrage Type </th>
                                <th> Demurrage  Amount</th>
                                <th> Demurrage Date </th>
                                <th> Demurage Received Date </th>
                                <th> Action </th>
                            </tr>
                            </thead>
                            <tbody>
                                @php $i=1; @endphp
                                @foreach ($cargo_trips as $cargo_trip)
                                <tr>
                                
                                    <td>  {{ $i }} </td>
                                    <td> Cargo Id: {{ @$cargo_trip->trip_auto_id }} <br>  
                                         Cargo Name: {{ @$cargo_trip->cargo->name }}  <br>
                                         Agency Name: {{ @$cargo_trip->agency->name }}  <br>
                                         Loaded From: {{ @$cargo_trip->des_form }} <br>
                                         Unloaded To:{{ @$cargo_trip->des_to }} <br>
                                         Loaded Date:{{ @$cargo_trip->selling_date }} <br>
                                         Unloaded Date:{{ @$cargo_trip->unload_date }} 
                                    </td>
                                    <td> {{ @$cargo_trip->damurrage->demurrage_type }} </td>
                                    <td> {{ @$cargo_trip->demurrage_amount }} </td>
                                    <td> {{ @$cargo_trip->demurrage_date }} </td>
                                    <td> {{ $cargo_trip->demurrage_received_date }} </td>
                                    
                                    
                                    <td>
                                    @if($cargo_trip->damarage_status == 1)
                                    <a class="btn btn-sm btn-success" title=""  href="#"><i class="fa fa-pencil-square-o"></i>Paid</a>
                                   @else
                                   <form action="{!! action('GetDemurrageController@update', ['id' => $cargo_trip->id]) !!}" method="POST">
                                            @csrf 
                                        <input type="hidden" name="_method" value="put">
                                        <button class="btn btn-sm btn-warning" onclick="return deleteFunction()" type="submit">Unpaid</button>
                                    </form>
                                   <!-- <a class="btn btn-sm btn-warning" title="" data-original-title="Edit" href="{{ route('cargo_trips.edit',$cargo_trip->id) }}"><i class="fa fa-pencil-square-o"></i>Unpaid</a> -->
                                   @endif
                                    </td>

                                @php $i++; @endphp
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <!-- //data on table -->

                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            
                        </nav>
                    </div>
                </div>
            </div>
        </div>
            
        @include('layouts.footers.auth')
    </div>
@endsection
@section('script')
<script>
    function deleteFunction(){
        if(!confirm("Are you sure to paid this?"))
        event.preventDefault();
    }
</script>
@endsection