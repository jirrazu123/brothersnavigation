@extends('layouts.app', ['title' => __('Create Demurrage')])

@section('content')
    @include('users.partials.header', ['title' => __('Create Demurrage')])   

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Create Demurrage') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('demurrages.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('demurrages.store') }}" autocomplete="off">
                            @csrf
                            
                            <div class="pl-lg-4">
                            <div class="form-row">

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="demurrage_type">{{ __('Demurrage Typoe') }}</label>
                                    <input type="text" name="demurrage_type" id="demurrage_type" class="form-control form-control-alternative" placeholder="{{ __('Demurrage Typoe') }}" required autofocus>
                                </div>

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="sort_des">{{ __('Short Description') }}</label>
                                    <textarea rows="4" cols="50" name="sort_des" id="sort_des" class="form-control form-control-alternative" placeholder="{{ __('Short Description') }}" required autofocus ></textarea>

                                </div>
                                
                                <div class="form-group col-md-12">
                                    <h3>Status</h3>
                                    @php
                                    if(@$cargo_expense_head->status){
                                    $active = 'checked';
                                    $inactive = '';
                                    }else{
                                      $active = '';
                                      $inactive = 'checked';
                                   }
                                  @endphp
                                  
                                  <div class="custom-control custom-radio mb-3">
                                  <input name="status" class="custom-control-input" id="status1" type="radio" value="0" {{ $inactive }}>
                                  <label class="custom-control-label" for="status1">inactive</label>
                                </div>
                                <div class="custom-control custom-radio mb-3">
                                  <input name="status" class="custom-control-input" value="1" id="status2"  type="radio"  {{ $active.(!Request::segment(3)) ? 'checked' : ''}}>
                                  <label class="custom-control-label" for="status2">active</label>
                                </div>
                                </div>
                                
                            </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        @include('layouts.footers.auth')
    </div>
@endsection