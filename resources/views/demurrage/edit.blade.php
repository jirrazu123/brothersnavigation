@extends('layouts.app', ['title' => __('Update Demurrage Information')])

@section('content')
    @include('users.partials.header', ['title' => __('Update Demurrage Information')])   

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Update Demurrage Information') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('demurrages.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{!! action('DemurrageController@update', ['id' => $demurrage->id]) !!}" autocomplete="off">
                            @csrf
                            <input type="hidden" name="_method" value="PUT">
                            <div class="pl-lg-4">
                            <div class="form-row">

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="demurrage_type">{{ __('Demurrage Type') }}</label>
                                    <input type="text" name="demurrage_type" id="demurrage_type" value="{{ $demurrage->demurrage_type }}" class="form-control form-control-alternative"  required autofocus>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="sort_des">{{ __('Short Description') }}</label>
                                    <textarea rows="4" cols="50" name="sort_des" value="{{ $demurrage->sort_dec }}" id="sort_des" class="form-control form-control-alternative" placeholder="{{ __('Short Description') }}" required autofocus >{{ $demurrage->sort_des }}</textarea>

                                </div>
                                @php
                                    if(@$demurrage->status){
                                    $active = 'checked';
                                    $inactive = '';
                                    }else{
                                      $active = '';
                                      $inactive = 'checked';
                                   }
                                  @endphp

                                <div class="form-group col-md-12">
                                    <h3>Status</h3>
                                    <div class="custom-control custom-radio mb-3">
                                   
                                    <input name="status" class="custom-control-input" value="1" @php echo @$active @endphp id="radio-1" type="radio">
                                    <label class="custom-control-label" for="radio-1">Active</label>
                                    </div>
                                    <div class="custom-control custom-radio mb-3">
                                    <input name="status" class="custom-control-input" value='0' @php echo @$inactive @endphp  id="radio-2" type="radio">
                                    <label class="custom-control-label" for="radio-2">Inactive</label>
                                    </div>
                                </div>
                                
                            </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        @include('layouts.footers.auth')
    </div>
@endsection