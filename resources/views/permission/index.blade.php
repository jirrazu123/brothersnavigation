@extends('layouts.app', ['title' => __('Permission Information')])

@section('content')
    @include('layouts.headers.cards')

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Permission') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('permissions.create') }}" class="btn btn-sm btn-primary">{{ __('Add Permission') }}</a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-12">
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                    </div>

                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th>{{ __('#SL') }}</th>
                                    <th>{{ __('Label') }}</th>
                                    <th>{{ __('Permission Name') }}</th>
                                    <th></th>
                                    <th class="text-right">{{__('Action') }}</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($permissions as $key=>$permission)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $permission->label }}</td>
                                    <td>{{ $permission->permission_name }}</td>
                                    <td class="text-right">
                                        <a href="{{route('permissions.edit', array('id'=>$permission->id)) }}" class="btn btn-sm btn-info">Edit</a>
                                        
                                    </td>
                                    <td>
                                        <form action="{{ route('permissions.destroy',  array('id'=>$permission->id)) }}" method="POST">
                                        @csrf 
                                        <input type="hidden" name="_method" value="DELETE"> 
                                        <button class="btn btn-sm btn-danger" type="submit">Delete</button>
                                        </form>
                                    </td>
                                    
                                </tr>
                                @endforeach
                                   
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            
                        </nav>
                    </div>
                </div>
            </div>
        </div>
            
        @include('layouts.footers.auth')
    </div>
@endsection