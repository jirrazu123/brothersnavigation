@extends('layouts.app', ['title' => __('Permission Entry')])

@section('content')
    @include('users.partials.header', ['title' => __('Permission Information')])   

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Permission') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('permissions.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('permissions.store') }}" autocomplete="off">
                            @csrf
                            
                            <h6 class="heading-small text-muted mb-4">{{ __('Cargo Expense Head information') }}</h6>
                            <div class="pl-lg-4">
                            <div class="form-row">

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="label">{{ __('Label') }}</label>
                                    <input type="text" name="label" id="label" class="form-control form-control-alternative" placeholder="{{ __('Label') }}" required autofocus>

                                </div>

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="permission_name">{{ __('Permission Name') }}</label>
                                    <input type="text" name="permission_name" id="permission_name" class="form-control form-control-alternative" placeholder="{{ __('Permission Name') }}" required autofocus>

                                </div>
                                
                                
                                
                            </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        @include('layouts.footers.auth')
    </div>
@endsection