@extends('layouts.app', ['title' => __('Create Trip Expense')])

@section('content')
    @include('users.partials.header', ['title' => __('Create Trip Expense')])   

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Create Trip Expenses') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('expenses.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('hotel_expenses.store') }}" autocomplete="off">
                            @csrf
                            <div class="pl-lg-4">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Select</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><input type="checkbox" name="record"></td>
                                            <td>
                                            </td>

                                            <td>
                                            <div class="form-group">
                                                
                                                <input type="number" name="amount[]" id="amount" class="form-control form-control-alternative" placeholder="{{ __('Enter amount') }}" value="" required="">
                                                
                                            </div>
                                            </td>
                                        </tr>
                                        
                                    </tbody>
                                </table>  
                                <div class="col-md-12 py-3 text-right">
                                    <span id="add-row" class="add-row btn btn-sm btn-success pull-left">Add Row</span>
                                    <span id='delete_row' class="delete-row btn btn-sm btn-danger pull-right">Delete Row</span>
                                </div>                          
                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        @include('layouts.footers.auth')
    </div>
@endsection

@section('script')
<script type="text/javascript">
$(document).ready(function(){
  $(document).on('click', '#add',function(){
      $('#new_head').append(($('#expense_mult').html()));
      $('#new_head').append($('<div class="cross col-md-1 mt-5"><i class="fas fa-times btn btn-sm btn-warning"></i></div>'));
      $('#new_head').css('display',''); 
  })
  $(document).on('click', '.cross', function(){
    $(this).prev().remove();
    $(this).prev().remove();
    $(this).remove();
  }) 
});


 $(document).ready(function(){
        $(".add-row").click(function(){
            var name = $("#name").val();
            var email = $("#email").val();
            var markup = "<tr><td><input type='checkbox' name='record'></td><td>" + name + "</td><td>" + email + "</td></tr>";
            $("table tbody").append(markup);
        });
        
        // Find and remove selected table rows
        $(".delete-row").click(function(){
            $("table tbody").find('input[name="record"]').each(function(){
            	if($(this).is(":checked")){
                    $(this).parents("tr").remove();
                }
            });
        });
    });
</script>
@endsection