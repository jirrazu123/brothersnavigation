@extends('layouts.app', ['title' => __('Agencies Create')])

@section('content')
    @include('users.partials.header', ['title' => __('Create Agencies Information')])   

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Agencies Create') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('agencies.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('agencies.store') }}" autocomplete="off">
                            @csrf
                            <div class="pl-lg-4">
                            <div class="form-row">

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="input-name">{{ __('Name') }}</label>
                                    <input type="text" name="name" id="input-name" class="form-control form-control-alternative" placeholder="{{ __('Name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                
                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="input-email">{{ __('Email') }}</label>
                                    <input type="email" name="email" id="input-email" class="form-control form-control-alternative" placeholder="{{ __('Email') }}"  autofocus>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="input-phone">{{ __('Phone') }}</label>
                                    <input type="text" name="phone" id="input-phone" class="form-control form-control-alternative" placeholder="{{ __('Phone') }}" required autofocus>

                                    @if ($errors->has('phone'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('phone') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="input-contact_person">{{ __('Contact Person') }}</label>
                                    <input type="text" name="contact_person" id="input-contact_person" class="form-control form-control-alternative" placeholder="{{ __('Contact Person') }}" required autofocus>

                                    @if ($errors->has('contact_person'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('contact_person') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                
                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="input-address">{{ __('Address') }}</label>
                                    <textarea rows="4" cols="50" name="address" id="input-address" class="form-control form-control-alternative" placeholder="{{ __('Address') }}" required autofocus></textarea>

                                    @if ($errors->has('address'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('address') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="input-contact_no">{{ __('Contact No') }}</label>
                                    <input type="text" name="contact_no" id="input-contact_no" class="form-control form-control-alternative" placeholder="{{ __('Contact No') }}" required autofocus>

                                    @if ($errors->has('contact_no'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('contact_no') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="input-current_balance">{{ __('Current Balance') }}</label>
                                    <input type="number" name="current_balance" id="input-current_balance" class="form-control form-control-alternative" placeholder="{{ __('Current Balance') }}" required autofocus>

                                    @if ($errors->has('current_balance'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('current_balance') }}</strong>
                                        </span>
                                    @endif
                                </div>
                              
                                <div class="form-group col-md-12">
                                    <h3>Status</h3>
                                    @php
                                    if(@$product->status){
                                    $active = 'checked';
                                    $inactive = '';
                                    }else{
                                      $active = '';
                                      $inactive = 'checked';
                                   }
                                  @endphp
                                  
                                  <div class="custom-control custom-radio mb-3">
                                  <input name="status" class="custom-control-input" id="status1" type="radio" value="0" {{ $inactive }}>
                                  <label class="custom-control-label" for="status1">inactive</label>
                                </div>
                                <div class="custom-control custom-radio mb-3">
                                  <input name="status" class="custom-control-input" value="1" id="status2"  type="radio"  {{ $active.(!Request::segment(3)) ? 'checked' : ''}}>
                                  <label class="custom-control-label" for="status2">active</label>
                                </div>
                                </div>
                                
                            </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        @include('layouts.footers.auth')
    </div>
@endsection