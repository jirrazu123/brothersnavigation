@extends('layouts.app', ['title' => __('Agencies Entry')])

@section('content')
    @include('layouts.headers.cards')

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Agencies') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('agencies.create') }}" class="btn btn-sm btn-primary">{{ __('Add Agencies') }}</a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-12">
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                    </div>

                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">{{ __('#SL') }}</th>
                                    <th scope="col">{{ __('Agencies Name') }}</th>
                                    <th scope="col">{{ __('Address') }}</th>
                                    <th scope="col">{{ __('Phone') }}</th>
                                    <th scope="col">{{ __('Con Person') }}</th>
                                    <th scope="col">{{ __('Con Phone') }}</th>
                                    <th scope="col">{{ __('Balance') }}</th>
                                    <th scope="col"></th>
                                    <th scope="col">{{__('Action') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    @$status=['inactive','active'];
                                @endphp
                                @foreach($agencys as $key=>$agency)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$agency->name}}</td>
                                        <td>{{$agency->address}}</td>
                                        <td>{{$agency->phone}}</td>
                                        <td>{{$agency->contact_person}}</td>
                                        <td>{{$agency->contact_no}}</td>
                                        <td>{{$agency->current_balance}}</td>
                                        <td class="text-right">
                                            <a href="{{ route('agencies.edit', array('id'=>$agency->id ))}}" class="btn btn-sm btn-info">Edit</a>
                                        </td>
                                        <td>
                                            <form action="{{ route('agencies.destroy',  array('id'=>$agency->id)) }}" method="POST">
                                                @csrf 
                                                <input type="hidden" name="_method" value="DELETE">
                                                <button class="btn btn-sm btn-danger" onclick="return myFunction();" type="submit">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                    
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            
                        </nav>
                    </div>
                </div>
            </div>
        </div>
            
        @include('layouts.footers.auth')
    </div>
@endsection

@section('script')
<script>
  function myFunction() {
      if(!confirm("Are You Sure to delete this?"))
      event.preventDefault();
  }
 </script>
@endsection