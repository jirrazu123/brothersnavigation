@extends('layouts.app', ['title' => __('Create Cargo/Coaster Expense')])

@section('content')
    @include('users.partials.header', ['title' => __('Create Cargo/Coaster Expense')])   

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Cargo/Coaster Expenses Create') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('cargo_expenses.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('cargo_expenses.store') }}" autocomplete="off">
                            @csrf
                            
                            
                            <div class="pl-lg-4">
                            <div class="form-row">
                                
                            <div class="form-group col-md-12">
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Cargo/Coaster Trip</label>
                                    <select class="form-control" name="cargo_trip_id" id="cargo_trip_id" required="">
                                        <option value="">---Select Trip---</option>>
                                        @foreach($cargo_trips as $cargo_trip)
                                            <option value="{{ $cargo_trip->id }}"
                                                {{ $cargo_trip->id==$cargo_trip_id ? 'selected' : '' }}>
                                              Trip id: {{ $cargo_trip->trip_auto_id }} Destination Form : {{ $cargo_trip->des_form }}  | Destination To : {{ $cargo_trip->des_to }}|{{ $cargo_trip->cargo_id }}| | Start Date : {{ date('d:m:Y', strtotime($cargo_trip->selling_date)) }} | End Date : {{ date('d:m:Y', strtotime($cargo_trip->unload_date)) }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>

                            </div>
                            <div class="col-md-12">
                                <div class="form-row">
                                    <div class="row col-md-10" id="expense_mult">
                                        
                                            <!-- js form add -->
                                            <div class="form-group col-md-5">
                                                <label class="form-control-label" for="head_id">{{ __('Cargo Expense Head') }}</label>
                                                <select class="form-control" name="head_id[]" id="head_id" required="">
                                                    <option value="">---select cargo trip expense head ---</option>>
                                                    @foreach($cargo_expense_heads as $cargo_expense_head)
                                                        <option value="{{ $cargo_expense_head->id }}">{{ $cargo_expense_head->name }}</option>
                                                    @endforeach
                                                </select>
                                                
                                            </div>

                                            <div class="form-group col-md-5">
                                                <label class="form-control-label" for="amount">{{ __('Amount') }}</label>
                                                <input type="number" name="amount[]" id="amount" class="form-control form-control-alternative" placeholder="{{ __('Enter amount') }}" value="" required="">
                                                
                                            </div>
                                            <!-- js from end -->
                                        
                                    </div>

                                    <div id="new_head" class="row col-md-10" style="display:none"> 
                                       
                                    </div>

                                    <div class="col-md-1 ">
                                        <p class="pt-3"></p>
                                        <span  id="add" class="btn btn-primary px-5" style="width:100%;"><i class="fas fa-plus"></i></span>
                                    </div>
                               
                            </div>
                           
                            
                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        @include('layouts.footers.auth')
    </div>
@endsection

@section('script')
<script type="text/javascript">
$(document).ready(function(){
  $(document).on('click', '#add',function(){
      $('#new_head').append(($('#expense_mult').html()));
      $('#new_head').append($('<div class="cross col-md-1 mt-5"><i class="fas fa-times btn btn-sm btn-warning"></i></div>'));
      $('#new_head').css('display',''); 
  })
  $(document).on('click', '.cross', function(){
    $(this).prev().remove();
    $(this).prev().remove();
    $(this).remove();
  }) 
});
</script>
@endsection