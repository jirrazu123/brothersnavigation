@extends('layouts.app', ['title' => __('Payment Management')])

@section('content')
    @include('layouts.headers.cards')

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Payment description') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('payments.create') }}" class="btn btn-sm btn-primary">{{ __('Add Payment') }}</a>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <div class="input-group input-group-alternative my-4">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-zoom-split-in  text-info"></i></span>
                                        </div>
                                        <input class="form-control form-control-alternative" placeholder="Enter Tirp id or name" type="text">
                                        <button class="btn btn-primary">Search</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4"></div>
                        </div>
                    </div>

                    <div class="col-12">
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                    </div>

               <!-- payment form description -->
              
               <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col"></th>
                                    <th scope="col">{{ __('Agency Name') }}</th>
                                    <th scope="col">{{ __('curreent Balance') }}</th>
                                    <th scope="col">{{ __('Payamount') }}</th>
                                    <th scope="col">{{ __('payment method') }}</th>
                                    <th scope="col">{{ __('payment Date') }}</th>
                                    <th scope="col">{{ __('Remark') }}</th>
                                    <th scope="col"></th>
                                    <th scope="col">{{__('Action') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    @$status=['inactive','active'];
                                @endphp
                                @foreach($payments as $key=>$payment)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $payment->agency->name }}</td>
                                        <td>{{ $payment->current_balance - $payment->payment_amount }}</td>
                                        <td>{{ $payment->payment_amount }}</td>
                                        <td>{{ $payment->payment_method }}</td>
                                        <td>{{ $payment->payment_date }}</td>
                                        <td>{{ $payment->remark }}</td>
                                      
                                        <td class="text-right">
                                            <a href="{{ route('payments.edit', array('id'=>$payment->id)) }}" class="btn btn-sm btn-info">Edit</a>
                                        </td>
                                        <td>
                                            <form action="{{ route('payments.destroy',  array('id'=>$payment->id)) }}" method="POST">
                                            @csrf 
                                            <input type="hidden" name="_method" value="DELETE">
                                            <button class="btn btn-sm btn-danger" onclick="return deleteFunction()" type="submit">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                    
                            </tbody>
                        </table>
                    </div>
               <!--// payment form description end -->
                <div class="card-footer py-4">
                    <nav class="d-flex justify-content-end" aria-label="...">
                       
                    </nav>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.footers.auth')
    </div>
@endsection
@section('script')
    <script>
        function deleteFunction(){
            if(!confirm('Are you sure to delete this?'))
                event.preventDefault();
        }
    </script>
@endsection