@extends('layouts.app', ['title' => __('Payment uipdate')])

@section('content')
    @include('layouts.headers.cards')

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Payment Update') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('payments.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <div class="input-group input-group-alternative my-4">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-zoom-split-in  text-info"></i></span>
                                        </div>
                                        <input class="form-control form-control-alternative" placeholder="Enter Tirp id or name" type="text">
                                        <button class="btn btn-primary">Search</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4"></div>
                        </div>
                    </div>

                    <div class="col-12">
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                    </div>

               <!-- payment create form-->
                    <div class="card-body">
                    <form method="POST" action="{!! action('PaymentController@update', ['id' =>$payment->id]) !!}" >
                            
                            @csrf
                            <input type="hidden" name="_method" value="PUT">
                            <div class="pl-lg-4">

                            
                            <!-- //payment_auto_generated_id -->

                            <div class="form-row">
                                
                                
                                <!--cargo id end-->
                                <!-- <div class="form-row"> -->
                                    <div class="form-group col-md-6">
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">Agency</label>
                                            <select class="form-control" name="agency_id" id="agency_id">
                                                @foreach($agencys as $agency)
                                                    <option value="{{$agency->id}}">{{$agency->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                    </div><!--agency id-->
                                    <div class="form-group col-md-6">
                                        <label for="current_balance">Agency Amount</label>
                                        <input type="number" name="current_balance" value="{{ $agency->id ? $agency->current_balance : 0 }}" id="current_balance" class=" form-control  form-control-alternative" placeholder="{{ __('current Balance') }}" readonly>
                                    </div><!--agency current Balance-->
                                <!-- </div> -->

                                <!--payment field-->
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <div class="form-group">
                                            <label for="payment_amount">Payment Amount</label>
                                            <input type="number" id="payment_amount" name="payment_amount" value="{{ $payment->payment_amount }}" class=" form-control  form-control-alternative" placeholder="{{ __('payment amount') }}">
                                        </div>
                                    </div><!--//Payment Amount-->
                                    <div class="form-group col-md-6">
                                        <label for="payment_date">Payment Date</label>
                                        <input type="date" name="payment_date" id="payment_date" value="{{ $payment->payment_date }}" class=" form-control  form-control-alternative" placeholder="{{ __('Payment date') }}">
                                    </div><!--//Payment Date-->
                                    <div class="form-group col-md-6">
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">Payment Method</label>
                                            <select class="form-control" name="payment_method" id="payment_method">
                                                <option>---Select pamyment method---</option>
                                                <option>Cash</option>
                                                <option>Bank</option>
                                                <option>bCash</option>
                                                <option>PayPal</option>
                                                <option>PayPal</option>
                                            </select>
                                        </div>
                                    </div><!--//Payment Method-->
                                    <div class="form-group col-md-6">
                                        <label for="remark">Remaek</label>
                                        <input type="text" name="remark" id="remark" class=" form-control  form-control-alternative" placeholder="{{ __('Write remark... ') }}">
                                    </div><!--//Payment Date-->
                                </div>
                                <!--//end payment field-->

                                @php
                                    if(@$payment->status){
                                    $active = 'checked';
                                    $inactive = '';
                                    }else{
                                      $active = '';
                                      $inactive = 'checked';
                                   }
                                  @endphp

                                <div class="form-group col-md-12">
                                    <h3>Status</h3>
                                    <div class="custom-control custom-radio mb-3">
                                   
                                    <input name="status" class="custom-control-input" value="1" @php echo @$active @endphp id="radio-1" type="radio">
                                    <label class="custom-control-label" for="radio-1">Active</label>
                                    </div>
                                    <div class="custom-control custom-radio mb-3">
                                    <input name="status" class="custom-control-input" value='0' @php echo @$inactive @endphp  id="radio-2" type="radio">
                                    <label class="custom-control-label" for="radio-2">Inactive</label>
                                    </div>
                                </div>
                            </div>
                                
                            </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
               <!--// payment create form end-->
                <div class="card-footer py-4">
                    <nav class="d-flex justify-content-end" aria-label="...">
                       
                    </nav>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.footers.auth')
    </div>
@endsection
@section('script')
    <script>
        function deleteFunction(){
            if(!confirm('Are you sure to delete this?'))
                event.preventDefault();
        }
    </script>
@endsection