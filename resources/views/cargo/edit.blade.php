@extends('layouts.app', ['title' => __('Cargo/Coaster Entry')])

@section('content')
    @include('users.partials.header', ['title' => __('Cargo/Coaster Information')])   

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Cargo/Coaster ') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('cargos.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{!! action('CargoController@update', ['id' => $cargo->id]) !!}" autocomplete="off">
                            @csrf
                            <input type="hidden" name="_method" value="PUT">
                            <h6 class="heading-small text-muted mb-4">{{ __('Cargo/Coaster information') }}</h6>
                            <div class="pl-lg-4">
                            <div class="form-row">

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="input-name">{{ __('Name') }}</label>
                                    <input type="text" name="name" id="input-name" value="{{ $cargo->name }}" class="form-control form-control-alternative" placeholder="{{ __('Name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group col-md-6">
                                    <label class="form-control-label" for="input-capacity">{{ __('Capacity') }}</label>
                                    <input type="text" name="capacity" id="input-capacity" value="{{ $cargo->capacity }}" class="form-control form-control-alternative" placeholder="{{ __('Capacity') }}" required autofocus>

                                    @if ($errors->has('capacity'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('capacity') }}</strong>
                                        </span>
                                    @endif
                                </div>
                              
                                @php
                                    if(@$cargo->status){
                                    $active = 'checked';
                                    $inactive = '';
                                    }else{
                                      $active = '';
                                      $inactive = 'checked';
                                   }
                                  @endphp

                                <div class="form-group col-md-12">
                                    <h3>Status</h3>
                                    <div class="custom-control custom-radio mb-3">
                                   
                                    <input name="status" class="custom-control-input" value="1" @php echo @$active @endphp id="radio-1" type="radio">
                                    <label class="custom-control-label" for="radio-1">Active</label>
                                    </div>
                                    <div class="custom-control custom-radio mb-3">
                                    <input name="status" class="custom-control-input" value='0' @php echo @$inactive @endphp  id="radio-2" type="radio">
                                    <label class="custom-control-label" for="radio-2">Inactive</label>
                                    </div>
                                </div>
                                
                                </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        @include('layouts.footers.auth')
    </div>
@endsection