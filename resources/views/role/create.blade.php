@extends('layouts.app', ['title' => __('Role Entry')])

@section('content')
    @include('users.partials.header', ['title' => __('Role Information')])   

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Role') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('roles.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('roles.store') }}" autocomplete="off">
                            @csrf
                            
                            <h6 class="heading-small text-muted mb-4">{{ __('Role information') }}</h6>
                            <div class="pl-lg-4">
                            <div class="form-row">

                                <div class="form-group col-md-6">
                                    <label class="form-control-name" for="name">{{ __('Name') }}</label>
                                    <input type="text" name="name" id="name" class="form-control form-control-alternative" placeholder="{{ __('Name') }}" required autofocus>

                                </div>
                            </div>
                            <div class="row">
                                    @foreach($permissions as $permission)
                                    <div class="col-md-4">
                                    <div class="form-group">
                                    <div class="custom-control custom-checkbox mb-3">
                                    <input class="custom-control-input" id="{{ $permission->permission_name }}" name="permissions[]" value="{{ $permission->id }}" type="checkbox">
                                    <label class="custom-control-label" for="{{ $permission->permission_name }}">{{ $permission->label }}</label>
                                    </div>
                                    </div>
                                    </div>
                                    @endforeach
                                </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        @include('layouts.footers.auth')
    </div>
@endsection