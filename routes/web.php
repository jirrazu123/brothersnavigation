<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
	Route::resource('basic_settings','BasicSettingController');
	Route::resource('incomes','IncomeHeadController');
	Route::resource('ships','ShipController');
	Route::resource('ship_expenses','ShipExpenseController');
	Route::resource('ship_expense_heads','ShipExpenseHeadController');
	Route::resource('trips','TripController');
	Route::resource('collections','CollectionController');
	Route::resource('expense_heads','ExpenseHeadController');
	Route::resource('expenses','ExpenseController');
	Route::resource('managers','ManagerController');
	Route::resource('cargos','CargoController');
	Route::resource('agencies', 'AgencyController');
	Route::resource('products', 'ProductController');
	Route::resource('customers', 'CustomerController');
	Route::resource('cargo_expense_heads', 'CargoExpenseHeadController');
	Route::resource('cargo_trips', 'CargoTripController');
	Route::resource('cargo_expenses', 'CargoExpenseController');
	Route::resource('permissions', 'PermissionController');
	Route::resource('roles', 'RoleController');
	Route::resource('demurrages', 'DemurrageController');
	Route::resource('get_demurrages', 'GetDemurrageController');
	Route::resource('payments', 'PaymentController');

	/*
	* Hotel Reservation Routes
	*/
	
	Route::resource('hotel_expense_heads', 'HotelExpenseHeadController');
	Route::resource('hotel_informations', 'HotelInformationController');
	Route::resource('hotel_expenses', 'HotelExpenseController');

	/*
	 * Employee management system  
	 */
	Route::resource('employees', 'EmployeeController');

});

